
import {Http,Headers,Response, RequestOptions} from "@angular/http";
import {Injectable} from "@angular/core";
import { HttpModule } from '@angular/http';
import 'rxjs/Rx';
import {Config} from "./config";
import {Subject} from "rxjs/Subject";
import {Observable} from "rxjs/Observable";
import {AlertController, NavController} from "ionic-angular";
import {HomePage} from "../pages/home/home";


@Injectable()

export class UserService
{

    constructor(public alertCtrl:AlertController) { };

    logOut()
    {
        window.localStorage.userid = '';
        window.localStorage.name = '';
        //this.navCtrl.push(HomePage);
    }

}


