import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ImageCarPage } from './image-car';

@NgModule({
  declarations: [
    ImageCarPage,
  ],
  imports: [
    IonicPageModule.forChild(ImageCarPage),
  ],
})
export class ImageCarPageModule {}
