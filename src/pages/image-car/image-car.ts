import {Component} from '@angular/core';
import {IonicPage, Loading, ModalController, NavController, NavParams} from 'ionic-angular';

import {Diagnostic} from "@ionic-native/diagnostic";
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import {CameraService} from "../../services/camera.service";
import {  ActionSheetController, ToastController, Platform, LoadingController } from 'ionic-angular';

import { File } from '@ionic-native/file';
import { Transfer, TransferObject } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { Camera ,CameraOptions} from '@ionic-native/camera';
import {CompanyService} from "../../services/CompanyService";
import {CompanyAdminPage} from "../company-admin/company-admin";
import {PopupPage} from "../popup/popup";

declare var cordova: any;


@IonicPage()
@Component({
    selector: 'page-image-car',
    templateUrl: 'image-car.html',
})
export class ImageCarPage {

    public Car;
    public CarImages: any[] = [];
    public host = "http://www.tapper.co.il/salecar/laravel/storage/app/public/"
    public image: string;
    public CarPlace;
    lastImage: string = null;
    loading: Loading;
    public Type:Number;
    //f2 : file:///storage/emulated/0/Android/data/io.ionic.starter18/cache/ : .Pic.jpg : 1510223388621.jpg
    //f2 : file:///storage/emulated/0/Android/data/io.ionic.starter18/cache/ : .Pic.jpg : 1510223666791.jpg

    constructor(public navCtrl: NavController, public modalCtrl: ModalController , public companyService:CompanyService, public navParams: NavParams ,public cameraService:CameraService, private diagnostic:Diagnostic, private Ftransfer: FileTransfer , private camera: Camera, private transfer: Transfer, private file: File, private filePath: FilePath, public actionSheetCtrl: ActionSheetController, public toastCtrl: ToastController, public platform: Platform, public loadingCtrl: LoadingController) {
        this.Car = navParams.get('Car');
        this.Type = navParams.get('Type');
        console.log("Car : " , this.Car)
        
        if(this.Type == 0)
            this.CarImages = this.Car['images'];
        else
        {
            this.CarImages = this.Car[0]['images'];
            this.Car = this.Car[0];
        }
        console.log("Img : " , this.CarImages)
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad ImageCarPage');
    }
    
    
    goToCarAdmin()
    {
        this.navCtrl.push(CompanyAdminPage)
    }
    
    delImage(i)
    {
        this.companyService.DelImageCar('DelImageCar',this.Car['id'], this.CarImages[i].id).then((data: any) => {
            console.log("R : " , data);
            this.CarImages = data;
        });
    }
    
    
    showPopup (i) {
        let Url = this.host+''+this.CarImages[i].url;
        let modal = this.modalCtrl.create(PopupPage, {url:Url}, {cssClass: 'FBPage'});
        modal.present();
        modal.onDidDismiss(data => {});
    }
    
    
    public presentActionSheet() {
        if(this.CarImages.length<6)
        {
            let actionSheet = this.actionSheetCtrl.create({
                title: 'Select Image Source',
                buttons: [
                    {
                        text: 'Load from Library',
                        handler: () => {
                            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
                        }
                    },
                    {
                        text: 'Use Camera',
                        handler: () => {
                            this.takePicture(this.camera.PictureSourceType.CAMERA);
                        }
                    },
                    {
                        text: 'Cancel',
                        role: 'cancel'
                    }
                ]
            });
            actionSheet.present();
        }
        else
            this.presentToast('אין אפשרות להוסיף יותר מ6 תמונות');
    }
    
    public takePicture(sourceType) {
        // Create options for the Camera Dialog
        var options = {
            quality: 60,
            sourceType: sourceType,
            saveToPhotoAlbum: false,
            correctOrientation: true,
            targetWidth: 1000,
            targetHeight: 1000,
            allowEdit: true
        };
        
        // Get the data of an image
        this.camera.getPicture(options).then((imagePath) => {
            // Special handling for Android library
            console.log("f0");
            if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
                this.filePath.resolveNativePath(imagePath)
                    .then(filePath => {
                        console.log("f01");
                        let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                        console.log("f02");
                        let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                        console.log("f03");
                        this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
                    });
            } else {
                var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
                var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
                this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
            }
        }, (err) => {
            this.presentToast('Error while selecting image.');
        });
    }
    
    
    // Create a new name for the image
    private createFileName() {
        console.log("f1");
        var d = new Date(),
            n = d.getTime(),
            newFileName =  n + ".jpg";
        return newFileName;
    }

// Copy the image to a local folder
    private copyFileToLocalDir(namePath, currentName, newFileName) {
        console.log("f2 : "+ namePath + " : " + currentName + " : " + newFileName);
        this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
            this.lastImage = newFileName;
            this.uploadImage();
        }, error => {
            this.presentToast('Error while storing file.');
        });
    }
    
    public presentToast(text) {
        console.log("f3");
        let toast = this.toastCtrl.create({
            message: text,
            duration: 3000,
            position: 'bottom',
            cssClass: "ToastClass"
        });
        toast.present();
    }

// Always get the accurate path to your apps folder
    public pathForImage(img) {
        if (img === null) {
            return '';
        } else {
            return cordova.file.dataDirectory + img;
        }
    }
    
    
    public uploadImage() {
        // Destination URL
        console.log("Up1  : ")
        var url = "http://www.tapper.co.il/salecar/laravel/public/api/GetFile";
        // File for Upload
        console.log("Up2  : " + this.lastImage + " : " + this.pathForImage(this.lastImage));
        var targetPath = this.pathForImage(this.lastImage);
        
        // File name only
        var filename = this.lastImage;
        
        var options = {
            fileKey: "file",
            fileName: filename,
            chunkedMode: false,
            mimeType: "multipart/form-data",
            params : {'fileName': filename, 'CarId':this.Car['id']}
        };
        console.log("Up3  : " + this.Car['id']);
        const fileTransfer: TransferObject = this.transfer.create();
        
        this.loading = this.loadingCtrl.create({
            content: 'Uploading...',
        });
        this.loading.present();
        
        // Use the FileTransfer to upload the image
        fileTransfer.upload(targetPath, url, options).then(data => {
            this.loading.dismissAll()
            this.callToImagesFromServer(data);
        }, err => {
            this.loading.dismissAll()
            this.presentToast('Error while uploading file.');
        });
    }
    
    public callToImagesFromServer(data)
    {
        console.log("GT : " , JSON.parse(data.response));
        this.CarImages = JSON.parse(data.response);
        //console.log(this.companyService.CompaniesArray);
       
        
       /* this.companyService.GetCompanyById('GetCompanyById').then((data: any) => {
            console.log("CarAdmin : ", data);
            this.Car = this.companyService.CompanyAdmin['cars'][this.CarPlace];
            this.CarImages = this.Car['images'];
        });*/
    }

}













































/*public presentActionSheet() {
       let actionSheet = this.actionSheetCtrl.create({
           title: 'Select Image Source',
           buttons: [
               {
                   text: 'Load from Library',
                   handler: () => {
                       this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
                   }
               },
               {
                   text: 'Use Camera',
                   handler: () => {
                       this.takePicture(this.camera.PictureSourceType.CAMERA);
                   }
               },
               {
                   text: 'Cancel',
                   role: 'cancel'
               }
           ]
       });
       actionSheet.present();
   }

   public takePicture(sourceType) {
       // Create options for the Camera Dialog
       var options = {
           quality: 60,
           sourceType: sourceType,
           saveToPhotoAlbum: false,
           correctOrientation: true,
           targetWidth: 1000,
           targetHeight: 1000,
           allowEdit: true
       };

       // Get the data of an image
       this.camera.getPicture(options).then((imagePath) => {
           // Special handling for Android library
           if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
               this.filePath.resolveNativePath(imagePath)
                   .then(filePath => {
                       let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                       let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                       this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
                   });
           } else {
               var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
               var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
               this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
           }

       }, (err) => {
           this.presentToast('Error while selecting image.');
       });
   }


   // Create a new name for the image
   private createFileName() {
       var d = new Date(),
           n = d.getTime(),
           newFileName =  n + ".jpg";
       return newFileName;
   }

// Copy the image to a local folder
   private copyFileToLocalDir(namePath, currentName, newFileName) {
       this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
           this.lastImage = newFileName;
           this.uploadImage();
       }, error => {
           this.presentToast('Error while storing file.');
       });
   }

   private presentToast(text) {
       let toast = this.toastCtrl.create({
           message: text,
           duration: 3000,
           position: 'top'
       });
       toast.present();
   }

// Always get the accurate path to your apps folder
   public pathForImage(img) {
       if (img === null) {
           return '';
       } else {
           return cordova.file.dataDirectory + img;
       }
   }

   public uploadImage() {
       // Destination URL
       var url = "http://www.tapper.co.il/salecar/laravel/public/api/GetFile";

       // File for Upload
       var targetPath = this.pathForImage(this.lastImage);

       // File name only
       var filename = this.lastImage;

       var options = {
           fileKey: "file",
           fileName: filename,
           chunkedMode: false,
           mimeType: "multipart/form-data",
           params : {'fileName': filename, 'CarId':this.Car['id']}
       };

       const fileTransfer: TransferObject = this.transfer.create();

       this.loading = this.loadingCtrl.create({
           content: 'Uploading...',
       });
       this.loading.present();

       // Use the FileTransfer to upload the image
       fileTransfer.upload(targetPath, url, options).then(data => {
           this.loading.dismissAll()
           this.companyService.GetCompanyById('GetCompanyById').then((data: any) => {
               console.log("CarAdmin : ", data);
           });
           /*this.presentToast('Image succesful uploaded.');
           this.Car['images'] = data.response;
           this.CarImages = this.Car['images'];
           alert(JSON.stringify(data.response));
       }, err => {
           this.loading.dismissAll()
           this.presentToast('Error while uploading file.');
       });
   }*/
