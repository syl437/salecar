import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CompanyAdminPage } from './company-admin';

@NgModule({
  declarations: [
    CompanyAdminPage,
  ],
  imports: [
    IonicPageModule.forChild(CompanyAdminPage),
  ],
})
export class CompanyAdminPageModule {}
