import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {CompaniesPage} from "../companies/companies";
import {ForgotPage} from "../forgot/forgot";
import {BasketPage} from "../basket/basket";
import {LoginPage} from "../login/login";
import {RegisterPage} from "../register/register";
import {CompanyAdminPage} from "../company-admin/company-admin";
import {SinglePage} from "../single/single";
import {CompanyService} from "../../services/CompanyService";
import {ManagerPage} from "../manager/manager";
import {SingleCarPage} from "../single-car/single-car";

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {

    public Global = 1;

    public Details = {
        'company_id': '',
        'model': '',
        'manufacturer': '',
        'size': '',
        'year': '',
        'km': '',
        'color': '',
        'hand': '',
        'price': '',
        'description': ''
    }

    public CarsTypesArray: any[] = [];
    public CarsUpTo30: any[] = [];
    public Companies: any[] = [];
    public host = "http://www.tapper.co.il/salecar/laravel/storage/app/public/"

    constructor(public navCtrl: NavController, public companyService: CompanyService) {
        this.companyService.GetCarTypes('GetCarTypes').then((data: any) => {
            this.CarsTypesArray = this.companyService.CarsTypesArray;
            console.log("CarTypes : ", data);
        });

        this.companyService.GetCarsUpTo30('GetCarsUpTo30').then((data: any) => {
            console.log("Cars : ", data);
            this.CarsUpTo30 = data;
        });

        this.companyService.GetAllCompanies('GetAllCompanies').then((data: any) => {
            console.log("Basket : ", data), this.Companies = data
        });
        
        if(window['Global'] == 1)
        {
            this.companyService.GetCompanyBySubDomain('GetCompanyBySubDomain', window['SubDomain']).then((data: any) => {
                console.log("SubDomain : ", data);
                this.navCtrl.push(SinglePage, {CompanyId: data})
            });
        }
    }

    GoToCompaniesPage() {
        //this.navCtrl.push(CompaniesPage);
        this.navCtrl.push(CompaniesPage);
    }

    GoToLoginPage() {
        if(window.localStorage.userid == '' || !window.localStorage.userid || window.localStorage.userid == 'undefined')
            this.navCtrl.push(LoginPage);
        else if(window.localStorage.isAdmin == 0)
            this.navCtrl.push(CompanyAdminPage);
        else
            this.navCtrl.push(ManagerPage);
    }

    openCarPage(Id)
    {
        let id = this.CarsUpTo30[Id]['id'];
        console.log("T : " , id);
        this.navCtrl.push(SingleCarPage, {CarId: id})
    }
    goToSinglePage(Id) {
        this.navCtrl.push(SinglePage, {CompanyId: Id})
    }
}
