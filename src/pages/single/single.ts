import {Component} from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import {SendToServerService} from "../../services/send_to_server";
import {BasketService} from "../../services/basket.service";
import {RegisterPage} from "../register/register";
import {Config} from "../../services/config";
import {BasketPage} from "../basket/basket";
import {PopupService} from "../../services/popups";
import {CategoriesPage} from "../categories/categories";
import {LangChangeEvent, TranslateService} from "ng2-translate";
import {CompanyService} from "../../services/CompanyService";
import {CarPage} from "../car/car";
import { SocialSharing } from '@ionic-native/social-sharing';
/**
 * Generated class for the SinglePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-single',
    templateUrl: 'single.html',
})
export class SinglePage {

    public Company: any[] = [];
    public Cars: any[] = [];
    public CompanyId = '';
    public CompanyPlace = '';
    //public host = "http://www.tapper.co.il/salecar/admin/public"
    public host = "http://www.tapper.co.il/salecar/laravel/storage/app/public/"
    public message = 'הזמין אותך לצפות בכרטיס ביקור המכיל את כל הרכבים שלו לצפייה לחץ כאן :';
    public link = '';
    public fullMessage = '';
    public fullImage = ''

    constructor(public navCtrl: NavController,private socialSharing: SocialSharing, public navParams: NavParams,public toastCtrl: ToastController, public companyService: CompanyService, private alertCtrl: AlertController, public Settings: Config, public BasketService: BasketService, public Popup: PopupService, private translate: TranslateService) {
        if(window['Global'] == 1)
            this.Company = navParams.get('CompanyId');
        else
        {
            this.CompanyPlace = navParams.get('CompanyId');
            this.Company = this.companyService.CompaniesArray[this.CompanyPlace];
        }

        this.CompanyId = this.Company['id'];
        this.companyService.GetAllCars('GetAllCars', this.CompanyId).then((data: any) => {
            console.log("Basket : ", data), this.Cars = data
        });

        this.link = "http://"+this.Company['subdomain']+".salecar.co.il"
        this.fullMessage =  this.Company['name']+this.message;
        this.fullImage = this.host+"/"+this.Company['logo'];
        console.log(this.fullMessage)
        console.log(this.Company['id'])
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad SinglePage');
    }

    goToCarPage(Id) {
        this.navCtrl.push(CarPage, {CarPlace: Id})
    }

    Face()
    {
        if(this.Company['facebook'] != null)
        {
            window.location.href = this.Company['facebook'];
        }
        else
        {
            this.presentToast('ללקוח זה לא קיים פייסבוק')
        }
    }

    waze()
    {
        if(this.Company['waze'] != null)
        {
            window.location.href = "waze://?ll="+this.Company['waze'];
        }
        else
        {
            this.presentToast('ללקוח זה לא קיים WAZE')
        }
    }

    site()
    {
        if(this.Company['website'] != null)
        {
            window.location.href = this.Company['website'];
        }
        else
        {
            this.presentToast('ללקוח זה לא קיים אתר אינטרנט')
        }
    }

    tel()
    {
        if(this.Company['phone'] != null)
        {
            window.location.href = "tel:"+this.Company['phone'];
        }
        else
        {
            this.presentToast('ללקוח זה לא קיים טלפון במערכת')
        }
    }

    mail()
    {
        if(this.Company['email'] != null)
        {
            window.location.href = "mailto:"+this.Company['email'];
        }
        else
        {
            this.presentToast('ללקוח זה לא קיים אימייל')
        }
    }

    share()
    {
        this.socialSharing.shareViaWhatsApp(this.fullMessage, this.fullImage, this.link).then(() => {
            // Success!
        }).catch(() => {
            // Error!
        });
    }


    public presentToast(text) {
        console.log("f3");
        let toast = this.toastCtrl.create({
            message: text,
            duration: 3000,
            position: 'bottom',
            cssClass: "ToastClass"
        });
        toast.present();
    }
}
