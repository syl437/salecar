import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {SendToServerService} from "../../services/send_to_server";
import {BasketPage} from "../basket/basket";
import {RegisterPage} from "../register/register";
import {ForgotPage} from "../forgot/forgot";
import {PopupService} from "../../services/popups";
import {LangChangeEvent, TranslateService} from "ng2-translate";
import {CompanyAdminPage} from "../company-admin/company-admin";
import {ManagerPage} from "../manager/manager";

/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */



@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  public Login =
  {
    "mail" : "",
    "password" : ""

  }
  public emailregex;
  public loginStatus;
  public serverResponse;

  constructor(public navCtrl: NavController, public navParams: NavParams,public alertCtrl:AlertController, public Server:SendToServerService, public Popup:PopupService, private translate: TranslateService) {
      translate.setDefaultLang('he');
     translate.onLangChange.subscribe((event: LangChangeEvent) => {

     });

  }

    loginUser()
    {
      this.emailregex = /\S+@\S+\.\S+/;

      if (this.Login.mail =="")
          this.Popup.presentAlert('הזן מייל תקין', 'מייל לא תקין','');

      else if (!this.emailregex.test(this.Login.mail)) {
          this.Popup.presentAlert('הזן מייל תקין', 'מייל לא תקין','');
          this.Login.mail= '';
      }
      else if (this.Login.password =="")
          this.Popup.presentAlert('הזן סיסמה תקין', 'סיסמה לא תקינה','');
      else
        {
           this.serverResponse = this.Server.LoginUser("UserLogin",this.Login).then(data=>{
           console.log("login response: ",  data);
            if (data == 0) {
                this.Popup.presentAlert('ניסיון נכשל', 'ניסיון ההתחברות נכשל אנא נסו שנית','');
                this.Login.password = '';
            }
            else{
                window.localStorage.userid = data['id'];
                window.localStorage.name = data['name'];
                window.localStorage.isAdmin = data['isadmin'];
                this.Login.mail= '';
                this.Login.password= '';

                if(data['isadmin'] == 0)
                    this.navCtrl.push(CompanyAdminPage);
                else
                    this.navCtrl.push(ManagerPage);
            }

           });
        }

    }

    goRegisterPage()
    {
        this.navCtrl.push(RegisterPage);
    }

    goForgotPage()
    {
        this.navCtrl.push(ForgotPage);
    }
  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }


}
