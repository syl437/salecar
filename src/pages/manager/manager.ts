import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {CompanyService} from "../../services/CompanyService";
import {EditCompanyPage} from "../edit-company/edit-company";
import {AddCompanyPage} from "../add-company/add-company";
import {LoginPage} from "../login/login";
import {CategoriesPage} from "../categories/categories";


/**
 * Generated class for the ManagerPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-manager',
    templateUrl: 'manager.html',
})
export class ManagerPage {

    public CompaniesArray: any[] = [];
    //public host = "http://www.tapper.co.il/salecar/admin/public"
    public host = "http://www.tapper.co.il/salecar/laravel/storage/app/public/"

    constructor(public navCtrl: NavController, public navParams: NavParams, public companyService: CompanyService) {
        this.companyService.GetAllCompanies('GetAllCompanies').then((data: any) => {
            console.log("Carray : ", data), this.CompaniesArray = data
        });
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad ManagerPage');
    }

    goToEditCompany(id) {
        this.navCtrl.push(EditCompanyPage , {CompanyId: id});
    }

    goToAddCompany()
    {
        this.navCtrl.push(AddCompanyPage);
    }

    LogOut() {
        window.localStorage.userid = '';
        window.localStorage.name = '';
        this.navCtrl.push(LoginPage);
    }

    GoToCategories()
    {
        this.navCtrl.push(CategoriesPage);
    };

}
