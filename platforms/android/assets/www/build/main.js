webpackJsonp([22],{

/***/ 152:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ForgotPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login_login__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_send_to_server__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_popups__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng2_translate__ = __webpack_require__(48);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the ForgotPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var ForgotPage = (function () {
    function ForgotPage(navCtrl, navParams, alertCtrl, Server, Popup, translate) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.Server = Server;
        this.Popup = Popup;
        this.translate = translate;
        this.Forgot = {
            "mail": ""
        };
        translate.setDefaultLang('he');
        translate.onLangChange.subscribe(function (event) {
        });
    }
    ForgotPage.prototype.sendPassword = function () {
        var _this = this;
        this.emailregex = /\S+@\S+\.\S+/;
        if (this.Forgot.mail == "")
            this.Popup.presentAlert(this.translate.instant("requiredtext"), this.translate.instant("inputmailtext"), '');
        else if (!this.emailregex.test(this.Forgot.mail)) {
            this.Popup.presentAlert(this.translate.instant("requiredtext"), this.translate.instant("requiredEmailText"), '');
            this.Forgot.mail = '';
        }
        else {
            this.serverResponse = this.Server.ForgotPassword("ForgotPassword", this.Forgot).then(function (data) {
                console.log("forgot response: ", data);
                if (data[0].status == 0) {
                    _this.Popup.presentAlert(_this.translate.instant("maildoesntexiststitletext"), _this.translate.instant("maildoesntexiststext"), '');
                    _this.Forgot.mail = '';
                }
                else {
                    _this.Popup.presentAlert(_this.translate.instant("passwordsenttitletext"), _this.translate.instant("passwordsenttext"), '');
                    _this.Forgot.mail = '';
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__login_login__["a" /* LoginPage */]);
                }
            });
        }
    };
    ForgotPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ForgotPage');
    };
    return ForgotPage;
}());
ForgotPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-forgot',template:/*ion-inline-start:"C:\Users\USER\Documents\github\salecar\salecar\src\pages\forgot\forgot.html"*/'<!--\n\n  Generated template for the RegisterPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n  <header></header>\n\n</ion-header>\n\n\n\n\n\n\n\n<ion-content padding>\n\n  <ion-item no-lines class="RegTitle">\n\n    <label>{{\'forgotpasstext\' | translate}}</label>\n\n  </ion-item>\n\n\n\n  <ion-list class="Inputs">\n\n\n\n    <ion-item>\n\n      <ion-label floating>{{\'emailtext\' | translate}}</ion-label>\n\n      <ion-input type="text" [(ngModel)]="Forgot.mail" name="mail" ></ion-input>\n\n    </ion-item>\n\n\n\n\n\n\n\n    <div style="width: 100%; margin-top: 2px;" align="center" (click)="sendPassword()">\n\n      <div style="width: 90%">\n\n        <button ion-button color="primary" style="width: 100%">{{\'sendtext\' | translate}}</button>\n\n      </div>\n\n    </div>\n\n\n\n\n\n\n\n  </ion-list>\n\n</ion-content>\n\n<ion-footer class="FooterClass">\n\n  <footer></footer>\n\n</ion-footer>'/*ion-inline-end:"C:\Users\USER\Documents\github\salecar\salecar\src\pages\forgot\forgot.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_3__services_send_to_server__["a" /* SendToServerService */], __WEBPACK_IMPORTED_MODULE_4__services_popups__["a" /* PopupService */], __WEBPACK_IMPORTED_MODULE_5_ng2_translate__["c" /* TranslateService */]])
], ForgotPage);

//# sourceMappingURL=forgot.js.map

/***/ }),

/***/ 153:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__basket_basket__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_config__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_send_to_server__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_basket_service__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_popups__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ng2_translate__ = __webpack_require__(48);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the RegisterPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var RegisterPage = (function () {
    function RegisterPage(navCtrl, navParams, Settings, alertCtrl, Server, BasketService, modalCtrl, Popup, translate) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.Settings = Settings;
        this.alertCtrl = alertCtrl;
        this.Server = Server;
        this.BasketService = BasketService;
        this.modalCtrl = modalCtrl;
        this.Popup = Popup;
        this.translate = translate;
        this.isAvaileble = false;
        this.TotalPrice = 0;
        this.Details = {
            'name': '',
            'mail': '',
            'password': '',
            'address': '',
            'phone': '',
            'newsletter': false
        };
        translate.onLangChange.subscribe(function (event) {
        });
        translate.setDefaultLang('he');
    }
    RegisterPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RegisterPage');
    };
    RegisterPage.prototype.ngOnInit = function () {
        this.ServerHost = this.Settings.ServerHost;
        this.products = this.BasketService.basket;
        this.TotalPrice = 0;
        for (var i = 0; i < this.products.length; i++) {
            this.TotalPrice += Number(this.products[i].Qnt) * Number(this.products[i].price);
        }
        console.log("cart:", this.products);
    };
    RegisterPage.prototype.doRegister = function () {
        var _this = this;
        this.emailregex = /\S+@\S+\.\S+/;
        if (this.Details.name.length < 3)
            this.Popup.presentAlert(this.translate.instant("missingFieldsText"), this.translate.instant("requiredFullnameText"), '');
        else if (this.Details.address.length < 3)
            this.Popup.presentAlert(this.translate.instant("missingFieldsText"), this.translate.instant("missingaddresstext"), '');
        else if (this.Details.mail == "")
            this.Popup.presentAlert(this.translate.instant("missingFieldsText"), this.translate.instant("requiredEmailText"), '');
        else if (!this.emailregex.test(this.Details.mail)) {
            this.Popup.presentAlert(this.translate.instant("missingFieldsText"), this.translate.instant("requiredEmailText"), '');
            this.Details.mail = '';
        }
        else if (this.Details.password == "")
            this.Popup.presentAlert(this.translate.instant("missingFieldsText"), this.translate.instant("missingpasswordtext"), '');
        else {
            this.serverResponse = this.Server.RegisterUser("RegisterUser", this.Details).then(function (data) {
                console.log("register response: ", data);
                if (data[0].status == 0) {
                    _this.Popup.presentAlert(_this.translate.instant("missingFieldsText"), _this.translate.instant("mailalreadyexiststext"), '');
                    _this.Details.mail = '';
                }
                else {
                    window.localStorage.userid = data[0].userid;
                    window.localStorage.name = _this.Details.name;
                    _this.Details.name = '';
                    _this.Details.mail = '';
                    _this.Details.address = '';
                    _this.Details.phone = '';
                    _this.Details.newsletter = false;
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__basket_basket__["a" /* BasketPage */]);
                }
            });
        }
    };
    return RegisterPage;
}());
RegisterPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-register',template:/*ion-inline-start:"C:\Users\USER\Documents\github\salecar\salecar\src\pages\register\register.html"*/'<!--\n\n  Generated template for the RegisterPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n  <header></header>\n\n</ion-header>\n\n\n\n\n\n\n\n<ion-content padding>\n\n    <ion-item no-lines class="RegTitle">\n\n      <label>{{\'registertitletext\' | translate}}</label>\n\n    </ion-item>\n\n\n\n  <ion-list class="Inputs">\n\n    <ion-item>\n\n      <ion-label floating>{{\'fullnametext\' | translate}}</ion-label>\n\n      <ion-input type="text" [(ngModel)]="Details.name" name="name" ></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label floating>{{\'addresstext\' | translate}}</ion-label>\n\n      <ion-input type="text" [(ngModel)]="Details.address" name="address" ></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label floating>{{\'emailtext\' | translate}}</ion-label>\n\n      <ion-input type="text" [(ngModel)]="Details.mail" name="mail" ></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label floating>{{\'passwordtext\' | translate}}</ion-label>\n\n      <ion-input type="text" [(ngModel)]="Details.password" name="password" ></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label floating>{{\'phonetext\' | translate}}</ion-label>\n\n      <ion-input type="tel" [(ngModel)]="Details.phone" name="phone" ></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item style="margin-top: 20px;" no-lines>\n\n      <ion-label>{{\'newslettertext\' | translate}}</ion-label>\n\n      <ion-checkbox color="dark" [(ngModel)]="Details.newsletter" ></ion-checkbox>\n\n    </ion-item>\n\n\n\n    <div style="width: 100%; margin-top: 20px;" align="center" (click)="doRegister()">\n\n      <div style="width: 90%">\n\n        <button ion-button color="primary" style="width: 100%">{{\'sendtext\' | translate}}</button>\n\n      </div>\n\n    </div>\n\n\n\n\n\n\n\n  </ion-list>\n\n</ion-content>\n\n<ion-footer class="FooterClass">\n\n  <footer></footer>\n\n</ion-footer>'/*ion-inline-end:"C:\Users\USER\Documents\github\salecar\salecar\src\pages\register\register.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__services_config__["a" /* Config */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_4__services_send_to_server__["a" /* SendToServerService */], __WEBPACK_IMPORTED_MODULE_5__services_basket_service__["a" /* BasketService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ModalController */], __WEBPACK_IMPORTED_MODULE_6__services_popups__["a" /* PopupService */], __WEBPACK_IMPORTED_MODULE_7_ng2_translate__["c" /* TranslateService */]])
], RegisterPage);

//# sourceMappingURL=register.js.map

/***/ }),

/***/ 154:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CompaniesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_CompanyService__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__single_single__ = __webpack_require__(68);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the CompaniesPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var CompaniesPage = (function () {
    function CompaniesPage(navCtrl, navParams, companyService) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.companyService = companyService;
        this.CompaniesArray = [];
        this.host = "http://www.tapper.co.il/salecar/admin/public";
        this.companyService.GetAllCompanies('GetAllCompanies').then(function (data) {
            console.log("Basket : ", data), _this.CompaniesArray = data;
        });
    }
    CompaniesPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CompaniesPage');
    };
    CompaniesPage.prototype.goToSinglePage = function (Id) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__single_single__["a" /* SinglePage */], { CompanyId: Id });
    };
    return CompaniesPage;
}());
CompaniesPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-companies',template:/*ion-inline-start:"C:\Users\USER\Documents\github\salecar\salecar\src\pages\companies\companies.html"*/'<!--\n\n  Generated template for the RegisterPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n  <header></header>\n\n</ion-header>\n\n\n\n\n\n\n\n<ion-content padding class="Content" style="margin-top:0px">\n\n\n\n    <div *ngFor="let item of CompaniesArray let i = index" >\n\n      <div class="ListRow" (click)="goToSinglePage(i)">\n\n          <div class="ListRight"  >\n\n              <img *ngIf="item.logo != null"  src="{{host}}/{{item.logo}}" style="width:100%; margin-top:-3px;"  />\n\n              <img *ngIf="item.logo == null"  src="images/logos.png" style="width:100%; margin-top:-3px;"  />\n\n          </div>\n\n          <div class="ListCenter"  >\n\n            <p class="bName">{{item.name}}</p>\n\n            <h3>{{item.phone}}</h3>\n\n            <h4>{{item.address}}</h4>\n\n          </div>\n\n\n\n          <div class="ListLeft">\n\n            <div>\n\n              <img  src="images/arrow.png" style="width:20px;" />\n\n            </div>\n\n          </div>\n\n        </div>\n\n\n\n        <div>\n\n          <img  src="images/line.png" style="width:100%; margin-top:-3px;" />\n\n        </div>\n\n    </div>\n\n</ion-content>\n\n<ion-footer class="FooterClass">\n\n  <footer></footer>\n\n</ion-footer>'/*ion-inline-end:"C:\Users\USER\Documents\github\salecar\salecar\src\pages\companies\companies.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_CompanyService__["a" /* CompanyService */]])
], CompaniesPage);

//# sourceMappingURL=companies.js.map

/***/ }),

/***/ 155:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CarPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_CompanyService__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__popup_popup__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the CarPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var CarPage = (function () {
    function CarPage(navCtrl, companyService, modalCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.companyService = companyService;
        this.modalCtrl = modalCtrl;
        this.navParams = navParams;
        this.CarPlace = '';
        this.Car = '';
        this.CarImages = [];
        //public host = "http://www.tapper.co.il/salecar/admin/public"
        this.host = "http://www.tapper.co.il/salecar/laravel/storage/app/public/";
        this.CarPlace = navParams.get('CarPlace');
        this.Car = this.companyService.CarsArray[this.CarPlace];
        this.CarImages = this.Car['images'];
        console.log("Car ", this.Car);
    }
    CarPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CarPage');
    };
    CarPage.prototype.showPopup = function (i) {
        var Url = this.host + '' + this.CarImages[i].url;
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__popup_popup__["a" /* PopupPage */], { url: Url }, { cssClass: 'FBPage' });
        modal.present();
        modal.onDidDismiss(function (data) { });
    };
    return CarPage;
}());
CarPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-car',template:/*ion-inline-start:"C:\Users\USER\Documents\github\salecar\salecar\src\pages\car\car.html"*/'<ion-header>\n\n  <header></header>\n\n</ion-header>\n\n\n\n\n\n<ion-content  class="Content" style="margin-top: 0px">\n\n  <div class="singleName">\n\n    {{Car.carModelName}} &nbsp;{{Car.carName}}\n\n  </div>\n\n\n\n  <div class="CarImages">\n\n    <div class="CarImageDiv" *ngFor="let image of CarImages let i = index" (click)="showPopup(i)">\n\n      <img *ngIf="i<6" src="{{host}}/{{image.url}}" />\n\n    </div>\n\n  </div>\n\n\n\n  <div class="c3Title">\n\n    פרטי הרכב\n\n  </div>\n\n  <div style="width:100%" align="center">\n\n    <div class="c3Line">\n\n    </div>\n\n  </div>\n\n  <div class="Info">\n\n    <div class="carInfo">\n\n      <div class="carInfoRight">\n\n        שנת ייצור :&nbsp;{{Car.year}}\n\n      </div>\n\n      <div class="carInfoRight">\n\n        בעלים קודמים :&nbsp;{{Car.hand}}\n\n      </div>\n\n    </div>\n\n    <div class="carInfo">\n\n      <div class="carInfoRight">\n\n        צבע הרכב :&nbsp;{{Car.color}}\n\n      </div>\n\n      <div class="carInfoRight">\n\n        קילומטר :&nbsp;{{Car.km}}\n\n      </div>\n\n    </div>\n\n    <div class="carInfo">\n\n      <div class="carInfoRight">\n\n        נפח מנוע :&nbsp;{{Car.size}}\n\n      </div>\n\n\n\n    </div>\n\n  </div>\n\n\n\n\n\n  <div align="center">\n\n    <div class="c3Info">\n\n      {{Car.description}}\n\n    </div>\n\n  </div>\n\n\n\n</ion-content>\n\n\n\n\n\n<ion-footer *ngIf="Car.price != 0 "  >\n\n  <ion-toolbar>\n\n    <ion-buttons  style="!important; text-align: center; padding: 0; margin: 0;"  >\n\n      <button ion-button icon-right color="royal" style="direction: rtl; font-size: 17px; font-weight: bold;  color: white;" (click)="LogOut()">\n\n\n\n      מחיר : {{Car.price|number}} ש"ח\n\n      </button>\n\n\n\n\n\n\n\n    </ion-buttons>\n\n  </ion-toolbar>\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\salecar\salecar\src\pages\car\car.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__services_CompanyService__["a" /* CompanyService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ModalController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]])
], CarPage);

//# sourceMappingURL=car.js.map

/***/ }),

/***/ 156:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddCarPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_send_to_server__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_popups__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__image_car_image_car__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_CompanyService__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the AddCarPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var AddCarPage = (function () {
    function AddCarPage(navCtrl, navParams, companyService, toastCtrl, Server, Popup) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.companyService = companyService;
        this.toastCtrl = toastCtrl;
        this.Server = Server;
        this.Popup = Popup;
        this.Details = {
            'company_id': '',
            'model': '',
            'manufacturer': '',
            'size': '',
            'year': '',
            'km': '',
            'color': '',
            'hand': '',
            'price': '',
            'description': ''
        };
        this.CarsTypesArray = [];
        this.CarsModelsArray = [];
        this.Details.company_id = navParams.get('CompanyId');
        this.CarsTypesArray = this.companyService.CarsTypesArray;
        this.Details.manufacturer = this.CarsTypesArray[0].id;
        this.CarTypeChange();
    }
    AddCarPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AddCarPage');
    };
    AddCarPage.prototype.AddCar = function () {
        var _this = this;
        if (this.Details.year.length < 3)
            this.presentToast('בחר שנת רכב');
        else if (this.Details.km.length < 3)
            this.presentToast('הכנס מספר קילומטר');
        else if (this.Details.color.length < 3)
            this.presentToast('בחר צבע הרכב');
        else if (this.Details.hand.length < 1)
            this.presentToast('בחר בעלים קודמים');
        else if (this.Details.price.length < 3)
            this.presentToast('הכנס מחיר');
        else {
            this.Server.AddCar("AddCar", this.Details).then(function (data) {
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__image_car_image_car__["a" /* ImageCarPage */], { Car: data, Type: 1 });
                //this.navCtrl.push(CompanyAdminPage);
            });
        }
    };
    AddCarPage.prototype.presentToast = function (text) {
        console.log("f3");
        var toast = this.toastCtrl.create({
            message: text,
            duration: 3000,
            position: 'bottom',
            cssClass: "ToastClass"
        });
        toast.present();
    };
    AddCarPage.prototype.CarTypeChange = function () {
        var _this = this;
        this.companyService.GetModels("GetModels", this.Details['manufacturer']).then(function (data) {
            _this.CarsModelsArray = data;
            _this.CarsTypesArray = _this.companyService.CarsTypesArray;
            _this.Details['model'] = _this.CarsModelsArray[0].id;
            console.log("Degem : ", _this.CarsModelsArray);
        });
    };
    return AddCarPage;
}());
AddCarPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-add-car',template:/*ion-inline-start:"C:\Users\USER\Documents\github\salecar\salecar\src\pages\add-car\add-car.html"*/'<ion-header>\n\n  <header></header>\n\n</ion-header>\n\n\n\n\n\n<ion-content >\n\n  <ion-item no-lines class="RegTitle">\n\n    <label>הוסף רכב חדש</label>\n\n  </ion-item>\n\n\n\n  <ion-list class="Inputs">\n\n    <ion-item>\n\n      <ion-label fixed>בחר סוג רכב</ion-label>\n\n      <ion-select [(ngModel)]="Details.manufacturer" (ngModelChange)="CarTypeChange()">\n\n        <ion-option *ngFor="let type of CarsTypesArray" value="{{type.id}}" >{{type.name}}</ion-option>\n\n      </ion-select>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label fixed>בחר דגם</ion-label>\n\n      <ion-select [(ngModel)]="Details.model" >\n\n        <ion-option *ngFor="let model of CarsModelsArray" value="{{model.id}}" >{{model.name}}</ion-option>\n\n      </ion-select>\n\n    </ion-item>\n\n\n\n\n\n    <ion-item>\n\n      <ion-label fixed>שנת הרכב</ion-label>\n\n      <ion-input type="tel" [(ngModel)]="Details.year" name="year" ></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label fixed>מספר קילומטר</ion-label>\n\n      <ion-input type="tel" [(ngModel)]="Details.km" name="km" ></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label fixed>בעלים קודמים</ion-label>\n\n      <ion-input type="tel" [(ngModel)]="Details.hand" name="hand" ></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label fixed>צבע הרכב</ion-label>\n\n      <ion-input type="text" [(ngModel)]="Details.color" name="color" ></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label fixed>נפח מנוע</ion-label>\n\n      <ion-input type="tel" [(ngModel)]="Details.size" name="size" ></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label fixed>מחיר הרכב</ion-label>\n\n      <ion-input type="tel" [(ngModel)]="Details.price" name="price" ></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label fixed>פרטים נוספים</ion-label>\n\n      <ion-textarea type="text" [(ngModel)]="Details.description" name="description" ></ion-textarea>\n\n    </ion-item>\n\n\n\n\n\n\n\n    <div style="width: 100%; margin-top: 20px;" align="center" (click)="AddCar()">\n\n      <div style="width: 90%">\n\n        <button ion-button color="primary" style="width: 100%">שלח פרטים</button>\n\n      </div>\n\n    </div>\n\n  </ion-list>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\salecar\salecar\src\pages\add-car\add-car.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_5__services_CompanyService__["a" /* CompanyService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* ToastController */], __WEBPACK_IMPORTED_MODULE_2__services_send_to_server__["a" /* SendToServerService */], __WEBPACK_IMPORTED_MODULE_3__services_popups__["a" /* PopupService */]])
], AddCarPage);

;
//# sourceMappingURL=add-car.js.map

/***/ }),

/***/ 157:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EditCarPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_CompanyService__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_send_to_server__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__company_admin_company_admin__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__image_car_image_car__ = __webpack_require__(94);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the EditCarPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var EditCarPage = (function () {
    function EditCarPage(navCtrl, navParams, companyService, Server) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.companyService = companyService;
        this.Server = Server;
        this.Details = {
            'model': '',
            'manufacturer': '',
            'size': '',
            'year': '',
            'km': '',
            'color': '',
            'hand': '',
            'price': '',
            'description': ''
        };
        this.CarsTypesArray = [];
        this.CarsModelsArray = [];
        this.CarPlace = navParams.get('CarId');
        this.Car = this.companyService.CompanyAdmin['cars'][this.CarPlace];
        this.Details = this.Car;
        this.CarsTypesArray = this.companyService.CarsTypesArray;
        this.CarTypeChange();
        console.log("TCar : ", this.Details);
    }
    EditCarPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad EditCarPage');
    };
    EditCarPage.prototype.EditCar = function () {
        var _this = this;
        this.Server.UpdateCar("UpdateCar", this.Details).then(function (data) {
            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__company_admin_company_admin__["a" /* CompanyAdminPage */]);
        });
    };
    EditCarPage.prototype.DelCar = function () {
        var _this = this;
        this.Server.DelCar("DelCar", this.Details).then(function (data) {
            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__company_admin_company_admin__["a" /* CompanyAdminPage */]);
        });
    };
    EditCarPage.prototype.goToImageCar = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__image_car_image_car__["a" /* ImageCarPage */], { Car: this.Car, Type: 0 });
    };
    EditCarPage.prototype.CarTypeChange = function () {
        var _this = this;
        this.companyService.GetModels("GetModels", this.Details['manufacturer']).then(function (data) {
            _this.CarsModelsArray = data;
            _this.CarsTypesArray = _this.companyService.CarsTypesArray;
            _this.Details['model'] = _this.CarsModelsArray[0].id;
            console.log("Degem : ", _this.CarsModelsArray);
        });
    };
    return EditCarPage;
}());
EditCarPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-edit-car',template:/*ion-inline-start:"C:\Users\USER\Documents\github\salecar\salecar\src\pages\edit-car\edit-car.html"*/'<ion-toolbar no-border-top class="ToolBarClass1">\n\n\n\n\n\n  <ion-buttons left>\n\n    <button navPop style="background-color: black">\n\n      <ion-icon style="color: white; font-size: 30px;" name="ios-arrow-back-outline"></ion-icon>\n\n    </button>\n\n  </ion-buttons>\n\n\n\n  <div class="ToolBarTitle">\n\n    <ion-title>\n\n      <div>\n\n        <img src="images/logo1.png" style="width: 130px; margin-top: 4px;">\n\n      </div>\n\n    </ion-title>\n\n  </div>\n\n\n\n  <ion-buttons right>\n\n    <button (click)="goToImageCar()" style="background-color: black">\n\n      <ion-icon style="color: white; font-size: 40px; margin-right:20px;" name="ios-camera-outline"></ion-icon>\n\n      <p style="color: white; font-size: 10px; margin-top: -7px;">הוסף תמונות</p>\n\n    </button>\n\n  </ion-buttons>\n\n</ion-toolbar>\n\n\n\n\n\n\n\n<ion-content style="margin-top: 20px" >\n\n  <ion-item no-lines class="RegTitle">\n\n    <label>פרטי הרכב</label>\n\n  </ion-item>\n\n\n\n  <ion-list class="Inputs">\n\n      <ion-item>\n\n          <ion-label fixed>בחר סוג רכב</ion-label>\n\n          <ion-select [(ngModel)]="Details.manufacturer" (ngModelChange)="CarTypeChange()">\n\n              <ion-option *ngFor="let type of CarsTypesArray" value="{{type.id}}" >{{type.name}}</ion-option>\n\n          </ion-select>\n\n      </ion-item>\n\n\n\n      <ion-item>\n\n        <ion-label fixed>בחר דגם</ion-label>\n\n        <ion-select [(ngModel)]="Details.model" >\n\n          <ion-option *ngFor="let model of CarsModelsArray" value="{{model.id}}" >{{model.name}}</ion-option>\n\n        </ion-select>\n\n      </ion-item>\n\n\n\n\n\n    <!-- <ion-item>\n\n      <ion-label fixed>שם הרכב</ion-label>\n\n      <ion-input type="text" [(ngModel)]="Details.model" name="model" ></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label fixed>דגם</ion-label>\n\n      <ion-input type="text" [(ngModel)]="Details.manufacturer" name="manufacturer" ></ion-input>\n\n    </ion-item>-->\n\n\n\n    <ion-item>\n\n      <ion-label fixed>שנת הרכב</ion-label>\n\n      <ion-input type="tel" [(ngModel)]="Details.year" name="year" ></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label fixed>מספר קילומטר</ion-label>\n\n      <ion-input type="tel" [(ngModel)]="Details.km" name="km" ></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label fixed>בעלים קודמים</ion-label>\n\n      <ion-input type="tel" [(ngModel)]="Details.hand" name="hand" ></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label fixed>צבע הרכב</ion-label>\n\n      <ion-input type="text" [(ngModel)]="Details.color" name="color" ></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label fixed>נפח מנוע</ion-label>\n\n      <ion-input type="tel" [(ngModel)]="Details.size" name="size" ></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label fixed>מחיר הרכב</ion-label>\n\n      <ion-input type="tel" [(ngModel)]="Details.price" name="price" ></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label fixed>פרטים נוספים</ion-label>\n\n      <ion-textarea type="text" [(ngModel)]="Details.description" name="description" ></ion-textarea>\n\n    </ion-item>\n\n\n\n\n\n\n\n    <div style="width: 100%; margin-top: 20px;" align="center" (click)="EditCar()">\n\n      <div style="width: 90%">\n\n        <button ion-button color="primary" style="width: 100%">שלח פרטים</button>\n\n      </div>\n\n    </div>\n\n      <div style="width: 100%; margin-top:0px;" align="center" (click)="DelCar()">\n\n          <div style="width: 90%">\n\n              <button ion-button color="primary" style="width: 100%; background-color: black; color: white">מחק רכב</button>\n\n          </div>\n\n      </div>\n\n\n\n  </ion-list>\n\n</ion-content>\n\n\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\salecar\salecar\src\pages\edit-car\edit-car.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_CompanyService__["a" /* CompanyService */], __WEBPACK_IMPORTED_MODULE_3__services_send_to_server__["a" /* SendToServerService */]])
], EditCarPage);

//# sourceMappingURL=edit-car.js.map

/***/ }),

/***/ 158:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EditCompanyPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_CompanyService__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_send_to_server__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__manager_manager__ = __webpack_require__(54);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the EditCompanyPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var EditCompanyPage = (function () {
    function EditCompanyPage(navCtrl, navParams, Server, companyService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.Server = Server;
        this.companyService = companyService;
        this.Details = {
            'model': '',
            'manufacturer': '',
            'size': '',
            'year': '',
            'km': '',
            'color': '',
            'hand': '',
            'price': '',
            'description': ''
        };
        this.CompanyPlace = '';
        this.Company = '';
        this.CompanyPlace = navParams.get('CompanyId');
        this.Company = this.companyService.CompaniesArray[this.CompanyPlace];
        console.log("CP : ", this.Company);
    }
    EditCompanyPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad EditCompanyPage');
    };
    EditCompanyPage.prototype.UpdateCompany = function () {
        var _this = this;
        this.Server.UpdateCompany("UpdateCompany", this.Company).then(function (data) {
            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__manager_manager__["a" /* ManagerPage */]);
        });
    };
    return EditCompanyPage;
}());
EditCompanyPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-edit-company',template:/*ion-inline-start:"C:\Users\USER\Documents\github\salecar\salecar\src\pages\edit-company\edit-company.html"*/'<ion-toolbar no-border-top class="ToolBarClass1">\n\n\n\n\n\n  <ion-buttons left>\n\n    <button navPop style="background-color: black">\n\n      <ion-icon style="color: white; font-size: 30px;" name="ios-arrow-back-outline"></ion-icon>\n\n    </button>\n\n  </ion-buttons>\n\n\n\n  <div class="ToolBarTitle">\n\n    <ion-title>\n\n      <div>\n\n        <img src="images/logo1.png" style="width: 130px; margin-top: 4px;">\n\n      </div>\n\n    </ion-title>\n\n  </div>\n\n\n\n</ion-toolbar>\n\n\n\n\n\n\n\n<ion-content style="margin-top: 75px" >\n\n  <ion-item no-lines class="RegTitle">\n\n    <label>ערוך חברה</label>\n\n  </ion-item>\n\n\n\n  <ion-list class="Inputs">\n\n    <ion-item>\n\n      <ion-label fixed>שם החברה</ion-label>\n\n      <ion-input type="text" [(ngModel)]="Company.name" name="name" ></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label fixed>כתובת</ion-label>\n\n      <ion-input type="text" [(ngModel)]="Company.address" name="address" ></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label fixed>טלפון</ion-label>\n\n      <ion-input type="tel" [(ngModel)]="Company.phone" name="phone" ></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label fixed>אימייל</ion-label>\n\n      <ion-input type="mail" [(ngModel)]="Company.email" name="email" ></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label fixed>subdomain</ion-label>\n\n      <ion-input type="text" [(ngModel)]="Company.subdomain" name="subdomain" ></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label fixed>סיסמה</ion-label>\n\n      <ion-input type="tel" [(ngModel)]="Company.password" name="password" ></ion-input>\n\n    </ion-item>\n\n\n\n\n\n    <ion-item>\n\n      <ion-label fixed>פרטים נוספים</ion-label>\n\n      <ion-textarea type="text" [(ngModel)]="Company.description" name="description" ></ion-textarea>\n\n    </ion-item>\n\n\n\n\n\n    <div style="width: 100%; margin-top: 20px;" align="center" (click)="UpdateCompany()">\n\n      <div style="width: 90%">\n\n        <button ion-button color="primary" style="width: 100%">שלח פרטים</button>\n\n      </div>\n\n    </div>\n\n\n\n  </ion-list>\n\n</ion-content>\n\n\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\salecar\salecar\src\pages\edit-company\edit-company.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__services_send_to_server__["a" /* SendToServerService */], __WEBPACK_IMPORTED_MODULE_2__services_CompanyService__["a" /* CompanyService */]])
], EditCompanyPage);

//# sourceMappingURL=edit-company.js.map

/***/ }),

/***/ 159:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddCompanyPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_send_to_server__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_CompanyService__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__manager_manager__ = __webpack_require__(54);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the AddCompanyPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var AddCompanyPage = (function () {
    function AddCompanyPage(navCtrl, navParams, Server, companyService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.Server = Server;
        this.companyService = companyService;
        this.Company = {
            'name': '',
            'address': '',
            'phone': '',
            'email': '',
            'subdomain': '',
            'password': '',
            'description': ''
        };
    }
    AddCompanyPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AddCompanyPage');
    };
    AddCompanyPage.prototype.AddCompany = function () {
        var _this = this;
        this.Server.AddCompany("AddCompany", this.Company).then(function (data) {
            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__manager_manager__["a" /* ManagerPage */]);
        });
    };
    return AddCompanyPage;
}());
AddCompanyPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-add-company',template:/*ion-inline-start:"C:\Users\USER\Documents\github\salecar\salecar\src\pages\add-company\add-company.html"*/'<ion-toolbar no-border-top class="ToolBarClass1">\n\n\n\n\n\n  <ion-buttons left>\n\n    <button navPop style="background-color: black">\n\n      <ion-icon style="color: white; font-size: 30px;" name="ios-arrow-back-outline"></ion-icon>\n\n    </button>\n\n  </ion-buttons>\n\n\n\n  <div class="ToolBarTitle">\n\n    <ion-title>\n\n      <div>\n\n        <img src="images/logo1.png" style="width: 130px; margin-top: 4px;">\n\n      </div>\n\n    </ion-title>\n\n  </div>\n\n\n\n</ion-toolbar>\n\n\n\n\n\n\n\n<ion-content style="margin-top:55px" >\n\n  <ion-item no-lines class="RegTitle">\n\n    <label>הוסף  חברה</label>\n\n  </ion-item>\n\n\n\n  <ion-list class="Inputs">\n\n    <ion-item>\n\n      <ion-label fixed>שם החברה</ion-label>\n\n      <ion-input type="text" [(ngModel)]="Company.name" name="name" ></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label fixed>כתובת</ion-label>\n\n      <ion-input type="text" [(ngModel)]="Company.address" name="address" ></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label fixed>טלפון</ion-label>\n\n      <ion-input type="tel" [(ngModel)]="Company.phone" name="phone" ></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label fixed>אימייל</ion-label>\n\n      <ion-input type="mail" [(ngModel)]="Company.email" name="email" ></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label fixed>subdomain</ion-label>\n\n      <ion-input type="text" [(ngModel)]="Company.subdomain" name="subdomain" ></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label fixed>סיסמה</ion-label>\n\n      <ion-input type="tel" [(ngModel)]="Company.password" name="password" ></ion-input>\n\n    </ion-item>\n\n\n\n\n\n    <ion-item>\n\n      <ion-label fixed>פרטים נוספים</ion-label>\n\n      <ion-textarea type="text" [(ngModel)]="Company.description" name="description" ></ion-textarea>\n\n    </ion-item>\n\n\n\n\n\n    <div style="width: 100%; margin-top: 20px;" align="center" (click)="AddCompany()">\n\n      <div style="width: 90%">\n\n        <button ion-button color="primary" style="width: 100%">שלח פרטים</button>\n\n      </div>\n\n    </div>\n\n\n\n  </ion-list>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\salecar\salecar\src\pages\add-company\add-company.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_send_to_server__["a" /* SendToServerService */], __WEBPACK_IMPORTED_MODULE_3__services_CompanyService__["a" /* CompanyService */]])
], AddCompanyPage);

//# sourceMappingURL=add-company.js.map

/***/ }),

/***/ 160:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CategoriesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_CompanyService__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__sub_categories_sub_categories__ = __webpack_require__(161);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the CategoriesPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var CategoriesPage = (function () {
    function CategoriesPage(navCtrl, navParams, companyService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.companyService = companyService;
        this.CarsTypesArray = [];
        this.CarsTypesArray = companyService.CarsTypesArray;
    }
    CategoriesPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CategoriesPage');
    };
    CategoriesPage.prototype.openSubCategory = function (i) {
        console.log("SendSub : " + i);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__sub_categories_sub_categories__["a" /* SubCategoriesPage */], { id: this.CarsTypesArray[i] });
    };
    return CategoriesPage;
}());
CategoriesPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-categories',template:/*ion-inline-start:"C:\Users\USER\Documents\github\salecar\salecar\src\pages\categories\categories.html"*/'<ion-toolbar no-border-top class="ToolBarClass1">\n\n\n\n\n\n  <ion-buttons left>\n\n    <button navPop style="background-color: black">\n\n      <ion-icon style="color: white; font-size: 30px;" name="ios-arrow-back-outline"></ion-icon>\n\n    </button>\n\n  </ion-buttons>\n\n\n\n  <div class="ToolBarTitle">\n\n    <ion-title>\n\n      <div (click)="LogOut()">\n\n        <img src="images/logo1.png" style="width: 130px; margin-top: 4px;">\n\n      </div>\n\n    </ion-title>\n\n  </div>\n\n\n\n  <ion-buttons right>\n\n\n\n  </ion-buttons>\n\n</ion-toolbar>\n\n\n\n\n\n\n\n\n\n<ion-content padding style="margin-top: 35px;">\n\n  <ion-list inset>\n\n    <button ion-item *ngFor="let item of CarsTypesArray let i=index" (click)="openSubCategory(i)" style="text-align: right">\n\n    {{ item.name }}\n\n  </button>\n\n\n\n  </ion-list>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\salecar\salecar\src\pages\categories\categories.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_CompanyService__["a" /* CompanyService */]])
], CategoriesPage);

//# sourceMappingURL=categories.js.map

/***/ }),

/***/ 161:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SubCategoriesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_CompanyService__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the SubCategoriesPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var SubCategoriesPage = (function () {
    function SubCategoriesPage(navCtrl, navParams, companyService, alertCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.companyService = companyService;
        this.alertCtrl = alertCtrl;
        this.CarsTypesArray = [];
        this.CategoryId = navParams.get('id');
        console.log("CR1 : ", this.CategoryId);
        this.companyService.GetModels("GetModels", this.CategoryId['id']).then(function (data) {
            _this.CarsTypesArray = data;
            console.log("CR : ", _this.CarsTypesArray);
        });
    }
    SubCategoriesPage.prototype.delCategory = function (i) {
        var _this = this;
        this.companyService.DelModel("DelModel", this.CarsTypesArray[i]['id'], this.CategoryId['id']).then(function (data) {
            _this.CarsTypesArray = data;
            console.log("CR : ", _this.CarsTypesArray);
        });
    };
    SubCategoriesPage.prototype.addCategory = function () {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'הוסף קטגוריה',
            message: "",
            cssClass: 'alertClass',
            inputs: [
                {
                    name: 'name',
                    placeholder: 'הכנס שם דגם'
                },
            ],
            buttons: [
                {
                    text: 'בטל',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'שלח',
                    handler: function (data) {
                        var name = data.name;
                        _this.companyService.AddModel("AddModel", _this.CategoryId['id'], data.name).then(function (data) {
                            _this.CarsTypesArray = data;
                            console.log("CR : ", _this.CarsTypesArray);
                        });
                    }
                }
            ]
        });
        prompt.present();
    };
    SubCategoriesPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SubCategoriesPage');
    };
    return SubCategoriesPage;
}());
SubCategoriesPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-sub-categories',template:/*ion-inline-start:"C:\Users\USER\Documents\github\salecar\salecar\src\pages\sub-categories\sub-categories.html"*/'<ion-toolbar no-border-top class="ToolBarClass1">\n\n\n\n\n\n  <ion-buttons left>\n\n    <button navPop style="background-color: black">\n\n      <ion-icon style="color: white; font-size: 30px;" name="ios-arrow-back-outline"></ion-icon>\n\n    </button>\n\n  </ion-buttons>\n\n\n\n  <div class="ToolBarTitle">\n\n    <ion-title>\n\n      <div (click)="LogOut()">\n\n        <img src="images/logo1.png" style="width: 130px; margin-top: 4px;">\n\n      </div>\n\n    </ion-title>\n\n  </div>\n\n\n\n  <ion-buttons right>\n\n    <button  style="background-color: black" (click)="addCategory()">\n\n      <ion-icon style="color: white; font-size: 40px; margin-right:20px;" name="ios-add-circle-outline"></ion-icon>\n\n      <p style="color: white; font-size: 10px; margin-top: 3px;">הוסף קטגוריה</p>\n\n    </button>\n\n  </ion-buttons>\n\n</ion-toolbar>\n\n\n\n\n\n<ion-content padding style="margin-top: 70px;">\n\n  <div style="padding-bottom: 200px">\n\n    <div *ngFor="let item of CarsTypesArray let i = index" style="" >\n\n      <div class="ListRow" >\n\n        <div class="ListCenter"  >\n\n          <p class="bName">{{item.name}}</p>\n\n        </div>\n\n\n\n        <div class="ListLeft" (click)="delCategory(i)">\n\n          <div >\n\n            <img  src="images/close.png" style="width:20px;" />\n\n          </div>\n\n        </div>\n\n      </div>\n\n\n\n      <div>\n\n        <img  src="images/line.png" style="width:100%; margin-top:-3px;" />\n\n      </div>\n\n    </div>\n\n  </div>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\salecar\salecar\src\pages\sub-categories\sub-categories.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_CompanyService__["a" /* CompanyService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
], SubCategoriesPage);

//# sourceMappingURL=sub-categories.js.map

/***/ }),

/***/ 162:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SingleCarPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_CompanyService__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__popup_popup__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the SingleCarPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var SingleCarPage = (function () {
    function SingleCarPage(navCtrl, companyService, modalCtrl, navParams) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.companyService = companyService;
        this.modalCtrl = modalCtrl;
        this.navParams = navParams;
        this.CarId = '';
        this.Car = '';
        this.CarImages = [];
        //public host = "http://www.tapper.co.il/salecar/admin/public"
        this.host = "http://www.tapper.co.il/salecar/laravel/storage/app/public/";
        this.CarId = navParams.get('CarId');
        this.companyService.GetCarById('GetCarById', this.CarId).then(function (data) {
            console.log("SingleCar : ", data), _this.Car = data, _this.CarImages = _this.Car['images'];
        });
        console.log("Car ", this.Car);
    }
    SingleCarPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CarPage');
    };
    SingleCarPage.prototype.showPopup = function (i) {
        var Url = this.host + '' + this.CarImages[i].url;
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__popup_popup__["a" /* PopupPage */], { url: Url }, { cssClass: 'FBPage' });
        modal.present();
        modal.onDidDismiss(function (data) { });
    };
    return SingleCarPage;
}());
SingleCarPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-single-car',template:/*ion-inline-start:"C:\Users\USER\Documents\github\salecar\salecar\src\pages\single-car\single-car.html"*/'<ion-header>\n\n  <header></header>\n\n</ion-header>\n\n\n\n\n\n<ion-content  class="Content" style="margin-top: 0px">\n\n  <div class="singleName">\n\n    {{Car.carModelName}} &nbsp;{{Car.carName}}\n\n  </div>\n\n\n\n  <div class="CarImages">\n\n    <div class="CarImageDiv" *ngFor="let image of CarImages let i = index" (click)="showPopup(i)" [ngClass]="{\'CarImageDiv33\' : (CarImages.length > 4), \'CarImageDiv50\':(CarImages.length == 2 || CarImages.length == 3 || CarImages.length == 4), \'CarImageDiv100\':(CarImages.length == 1)}">\n\n      <img *ngIf="i<6" src="{{host}}/{{image.url}}" />\n\n    </div>\n\n  </div>\n\n\n\n  <div class="c3Title">\n\n    פרטי הרכב\n\n  </div>\n\n  <div style="width:100%" align="center">\n\n    <div class="c3Line">\n\n    </div>\n\n  </div>\n\n  <div class="Info">\n\n    <div class="carInfo">\n\n      <div class="carInfoRight">\n\n        שנת ייצור :&nbsp;{{Car.year}}\n\n      </div>\n\n      <div class="carInfoRight">\n\n        בעלים קודמים :&nbsp;{{Car.hand}}\n\n      </div>\n\n    </div>\n\n    <div class="carInfo">\n\n      <div class="carInfoRight">\n\n        צבע הרכב :&nbsp;{{Car.color}}\n\n      </div>\n\n      <div class="carInfoRight">\n\n        קילומטר :&nbsp;{{Car.km}}\n\n      </div>\n\n    </div>\n\n    <div class="carInfo">\n\n      <div class="carInfoRight">\n\n        נפח מנוע :&nbsp;{{Car.size}}\n\n      </div>\n\n\n\n    </div>\n\n  </div>\n\n\n\n\n\n  <div align="center">\n\n    <div class="c3Info">\n\n      {{Car.description}}\n\n    </div>\n\n  </div>\n\n\n\n</ion-content>\n\n\n\n\n\n<ion-footer *ngIf="Car.price != 0 "  >\n\n  <ion-toolbar>\n\n    <ion-buttons  style="!important; text-align: center; padding: 0; margin: 0;"  >\n\n      <button ion-button icon-right color="royal" style="direction: rtl; font-size: 17px; font-weight: bold;  color: white;" (click)="LogOut()">\n\n\n\n        מחיר : {{Car.price|number}} ש"ח\n\n      </button>\n\n\n\n\n\n\n\n    </ion-buttons>\n\n  </ion-toolbar>\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\salecar\salecar\src\pages\single-car\single-car.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__services_CompanyService__["a" /* CompanyService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ModalController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]])
], SingleCarPage);

//# sourceMappingURL=single-car.js.map

/***/ }),

/***/ 163:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ShopPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_send_to_server__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__single_single__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_config__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the ShopPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var ShopPage = (function () {
    function ShopPage(navCtrl, navParams, Server, Settings) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.Server = Server;
        this.Settings = Settings;
        this.defaultLangage = '';
        this.defaultLangage = this.Settings.defaultLanguage;
    }
    ShopPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ShopPage');
    };
    ShopPage.prototype.ngOnInit = function () {
        this.ServerHost = this.Settings.ServerHost;
        this.CategoryId = this.navParams.get('cat');
        this.SubCatId = this.navParams.get('subcat');
        this.products = this.Server.ProductsArray[this.CategoryId].subcat[this.SubCatId].products;
        console.log("products:", this.products);
    };
    ShopPage.prototype.goToSinglePage = function (ProductId) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__single_single__["a" /* SinglePage */], { CategoryId: this.CategoryId, SubCatId: this.SubCatId, ProductId: ProductId });
    };
    return ShopPage;
}());
ShopPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-shop',template:/*ion-inline-start:"C:\Users\USER\Documents\github\salecar\salecar\src\pages\shop\shop.html"*/'\n\n<ion-header>\n\n    <header></header>\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <div>\n\n      <div class="Product" *ngFor="let product of products let i = index" (click)="goToSinglePage(i)">\n\n        <div class="ProductRight">\n\n          <img src="{{ServerHost+product.image}}" class="w100"  />\n\n        </div>\n\n\n\n        <div class="ProductCenter">\n\n            <span *ngIf="defaultLangage == \'he\'">{{product.title}}</span>\n\n            <span *ngIf="defaultLangage == \'en\'">{{product.title_english}}</span>\n\n              <p>{{product.low_price}}<span> {{\'nistext\' | translate}}  {{\'forkgtext\' | translate}}</span></p>\n\n        </div>\n\n\n\n        <div class="ProductLeft" >\n\n          <img src="images/buy.png" class="w100"  />\n\n        </div>\n\n      </div>\n\n    <div class="Line"></div>\n\n  </div>\n\n</ion-content>\n\n\n\n<ion-footer class="FooterClass">\n\n    <footer></footer>\n\n</ion-footer>'/*ion-inline-end:"C:\Users\USER\Documents\github\salecar\salecar\src\pages\shop\shop.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_send_to_server__["a" /* SendToServerService */], __WEBPACK_IMPORTED_MODULE_4__services_config__["a" /* Config */]])
], ShopPage);

//# sourceMappingURL=shop.js.map

/***/ }),

/***/ 174:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	return new Promise(function(resolve, reject) { reject(new Error("Cannot find module '" + req + "'.")); });
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 174;

/***/ }),

/***/ 20:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CompanyService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ServerUrl = "http://tapper.co.il/salecar/laravel/public/api/";
var CompanyService = (function () {
    function CompanyService(http, Settings) {
        this.http = http;
        this.Settings = Settings;
        this.CompaniesArray = [];
        this.CarsArray = [];
        this.CarsTypesArray = [];
        this.CompanyAdmin = [];
    }
    ;
    CompanyService.prototype.GetAllCompanies = function (url) {
        var _this = this;
        var body = new FormData();
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/x-www-form-urlencoded' });
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({ headers: headers });
        body.append('uid', this.Settings.UserId.toString());
        return this.http.post(ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) {
            console.log("Parray", data);
            _this.CompaniesArray = data;
        }).toPromise();
    };
    CompanyService.prototype.GetAllCars = function (url, id) {
        var _this = this;
        var body = new FormData();
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/x-www-form-urlencoded' });
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({ headers: headers });
        body.append('uid', this.Settings.UserId.toString());
        body.append('id', id);
        return this.http.post(ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) {
            console.log("Cars", data);
            _this.CarsArray = data;
        }).toPromise();
    };
    CompanyService.prototype.GetCompanyById = function (url) {
        var _this = this;
        console.log("GT1");
        var body = new FormData();
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/x-www-form-urlencoded' });
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({ headers: headers });
        console.log("GT2");
        body.append('uid', this.Settings.UserId.toString());
        console.log("GT3", window.localStorage.userid);
        body.append('id', window.localStorage.userid);
        return this.http.post(ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) {
            _this.CompanyAdmin = data;
            console.log("GT5");
        }).toPromise();
    };
    CompanyService.prototype.GetCompanyBySubDomain = function (url, sub) {
        var body = new FormData();
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/x-www-form-urlencoded' });
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({ headers: headers });
        body.append('sub', sub);
        return this.http.post(ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) {
        }).toPromise();
    };
    CompanyService.prototype.DelImageCar = function (url, carid, i) {
        var body = new FormData();
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/x-www-form-urlencoded' });
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({ headers: headers });
        body.append('carId', carid);
        body.append('imageId', i);
        return this.http.post(ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { }).toPromise();
    };
    CompanyService.prototype.GetCarTypes = function (url) {
        var _this = this;
        var body = new FormData();
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/x-www-form-urlencoded' });
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.post(ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { _this.CarsTypesArray = data; }).toPromise();
    };
    CompanyService.prototype.GetModels = function (url, manufacturer) {
        var body = new FormData();
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/x-www-form-urlencoded' });
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({ headers: headers });
        body.append('manufacturer', manufacturer);
        return this.http.post(ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { }).toPromise();
    };
    CompanyService.prototype.SendSms = function (url, message, phone) {
        var smsUrl = 'http://tapper.co.il/send_sms.php';
        var body = new FormData();
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/x-www-form-urlencoded' });
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({ headers: headers });
        body.append('message', message);
        body.append('phone', phone);
        return this.http.post(smsUrl, body).map(function (res) { return res; }).do(function (data) { }).toPromise();
    };
    CompanyService.prototype.GetCarsUpTo30 = function (url) {
        var body = new FormData();
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/x-www-form-urlencoded' });
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.post(ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { }).toPromise();
    };
    CompanyService.prototype.DelModel = function (url, id, carId) {
        var body = new FormData();
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/x-www-form-urlencoded' });
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({ headers: headers });
        body.append('id', id);
        body.append('carId', carId);
        return this.http.post(ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { }).toPromise();
    };
    CompanyService.prototype.AddModel = function (url, carId, data) {
        var body = new FormData();
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/x-www-form-urlencoded' });
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({ headers: headers });
        body.append('data', data);
        body.append('carId', carId);
        return this.http.post(ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { }).toPromise();
    };
    CompanyService.prototype.GetCarById = function (url, carId) {
        var body = new FormData();
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/x-www-form-urlencoded' });
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({ headers: headers });
        body.append('carId', carId);
        return this.http.post(ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { }).toPromise();
    };
    return CompanyService;
}());
CompanyService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_3__config__["a" /* Config */]])
], CompanyService);

;
//# sourceMappingURL=CompanyService.js.map

/***/ }),

/***/ 217:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/about/about.module": [
		628,
		21
	],
	"../pages/add-car/add-car.module": [
		613,
		20
	],
	"../pages/add-company/add-company.module": [
		617,
		19
	],
	"../pages/basket/basket.module": [
		622,
		18
	],
	"../pages/car/car.module": [
		624,
		17
	],
	"../pages/categories/categories.module": [
		619,
		16
	],
	"../pages/companies/companies.module": [
		626,
		15
	],
	"../pages/company-admin/company-admin.module": [
		615,
		14
	],
	"../pages/contact/contact.module": [
		630,
		13
	],
	"../pages/edit-car/edit-car.module": [
		614,
		12
	],
	"../pages/edit-company/edit-company.module": [
		616,
		11
	],
	"../pages/forgot/forgot.module": [
		611,
		10
	],
	"../pages/image-car/image-car.module": [
		612,
		9
	],
	"../pages/login/login.module": [
		621,
		8
	],
	"../pages/manager/manager.module": [
		620,
		7
	],
	"../pages/popup/popup.module": [
		610,
		6
	],
	"../pages/register/register.module": [
		623,
		5
	],
	"../pages/shop/shop.module": [
		629,
		4
	],
	"../pages/single-car/single-car.module": [
		627,
		3
	],
	"../pages/single/single.module": [
		625,
		2
	],
	"../pages/sub-categories/sub-categories.module": [
		618,
		1
	],
	"../pages/subcategory/subcategory.module": [
		631,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
module.exports = webpackAsyncContext;
webpackAsyncContext.id = 217;

/***/ }),

/***/ 23:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SendToServerService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ServerUrl = "http://tapper.co.il/salecar/laravel/public/api/";
var SendToServerService = (function () {
    function SendToServerService(http, Settings) {
        this.http = http;
        this.Settings = Settings;
    }
    ;
    SendToServerService.prototype.GetAllProducts = function (url) {
        var _this = this;
        var body = new FormData();
        body.append('uid', this.Settings.UserId.toString());
        return this.http.post(ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) {
            console.log("Parray", data), _this.ProductsArray = data;
        }).toPromise();
    };
    SendToServerService.prototype.GetAbout = function (url) {
        var _this = this;
        var body = new FormData();
        body.append('uid', this.Settings.UserId.toString());
        return this.http.post(ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) {
            _this.AboutArray = data;
        }).toPromise();
    };
    SendToServerService.prototype.GetCategories = function (url) {
        var _this = this;
        var body = new FormData();
        body.append('uid', this.Settings.UserId.toString());
        return this.http.post(ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) {
            _this.MainCategories = data;
            //console.log("Categories : " , data)
        }).toPromise();
    };
    SendToServerService.prototype.SendBasketToServer = function (url, Basket, sum, pay) {
        var userid = window.localStorage.userid;
        var body = 'userid=' + userid.toString() + '&basket=' + JSON.stringify(Basket) + '&sum=' + sum.toString() + '&pay=' + pay.toString();
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({
            headers: headers
        });
        return this.http.post(ServerUrl + '' + url, body, options).map(function (res) { return res; }).do(function (data) {
            console.log("Baskett : ", data);
        }).toPromise();
    };
    SendToServerService.prototype.sendContactDetails = function (url) {
        var body = 'name=' + this.Settings.ContactDetails['name'] + '&details=' + this.Settings.ContactDetails['details'] + '&mail=' + this.Settings.ContactDetails['mail'] + '&phone=' + this.Settings.ContactDetails['phone'];
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({
            headers: headers
        });
        return this.http.post(ServerUrl + '' + url, body, options).map(function (res) { return res; }).do(function (data) {
            console.log("Contact : ", data);
        }).toPromise();
    };
    SendToServerService.prototype.LoginUser = function (url, params) {
        var body = 'mail=' + params.mail + '&password=' + params.password;
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({
            headers: headers
        });
        return this.http.post(ServerUrl + '' + url, body, options).map(function (res) { return res.json(); }).do(function (data) {
            console.log("Login : ", data);
        }).toPromise();
    };
    SendToServerService.prototype.RegisterUser = function (url, params) {
        if (params.newsletter == true)
            this.newsletter = 1;
        else
            this.newsletter = 0;
        var body = 'name=' + params.name + '&mail=' + params.mail + '&password=' + params.password + '&address=' + params.address + '&phone=' + params.phone + '&newsletter=' + this.newsletter;
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({
            headers: headers
        });
        return this.http.post(ServerUrl + '' + url, body, options).map(function (res) { return res.json(); }).do(function (data) {
            console.log("Register: : ", data);
        }).toPromise();
    };
    SendToServerService.prototype.ForgotPassword = function (url, params) {
        var body = 'mail=' + params.mail;
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({
            headers: headers
        });
        return this.http.post(ServerUrl + '' + url, body, options).map(function (res) { return res.json(); }).do(function (data) {
            console.log("Forgot : ", data);
        }).toPromise();
    };
    SendToServerService.prototype.UpdateCar = function (url, params) {
        var body = 'company_id=' + params.company_id + '&color=' + params.color + '&description=' + params.description + '&hand=' + params.hand + '&id=' + params.id + '&km=' + params.km + '&manufacturer=' + params.manufacturer + '&model=' + params.model + '&price=' + params.price + '&size=' + params.size + '&year=' + params.year;
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({
            headers: headers
        });
        return this.http.post(ServerUrl + '' + url, body, options).map(function (res) { return res.json(); }).do(function (data) {
            console.log("Register: : ", data);
        }).toPromise();
    };
    SendToServerService.prototype.DelCar = function (url, params) {
        var body = 'id=' + params.id;
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({
            headers: headers
        });
        return this.http.post(ServerUrl + '' + url, body, options).map(function (res) { return res.json(); }).do(function (data) {
            console.log("Register: : ", data);
        }).toPromise();
    };
    SendToServerService.prototype.AddCar = function (url, params) {
        var body = 'company_id=' + params.company_id + '&color=' + params.color + '&description=' + params.description + '&hand=' + params.hand + '&km=' + params.km + '&manufacturer=' + params.manufacturer + '&model=' + params.model + '&price=' + params.price + '&size=' + params.size + '&year=' + params.year;
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({
            headers: headers
        });
        return this.http.post(ServerUrl + '' + url, body, options).map(function (res) { return res.json(); }).do(function (data) {
            console.log("Register: : ", data);
        }).toPromise();
    };
    SendToServerService.prototype.UpdateCompany = function (url, params) {
        var body = 'id=' + params.id + '&name=' + params.name + '&description=' + params.description + '&address=' + params.address + '&phone=' + params.phone + '&email=' + params.email + '&subdomain=' + params.subdomain + '&password=' + params.password;
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({
            headers: headers
        });
        return this.http.post(ServerUrl + '' + url, body, options).map(function (res) { return res.json(); }).do(function (data) {
            console.log("Register: : ", data);
        }).toPromise();
    };
    SendToServerService.prototype.AddCompany = function (url, params) {
        var body = 'name=' + params.name + '&description=' + params.description + '&address=' + params.address + '&phone=' + params.phone + '&email=' + params.email + '&subdomain=' + params.subdomain + '&password=' + params.password;
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({
            headers: headers
        });
        return this.http.post(ServerUrl + '' + url, body, options).map(function (res) { return res.json(); }).do(function (data) {
            console.log("Register: : ", data);
        }).toPromise();
    };
    return SendToServerService;
}());
SendToServerService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_3__config__["a" /* Config */]])
], SendToServerService);

;
//# sourceMappingURL=send_to_server.js.map

/***/ }),

/***/ 253:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CameraService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_file__ = __webpack_require__(127);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_transfer__ = __webpack_require__(128);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_file_path__ = __webpack_require__(129);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_camera__ = __webpack_require__(130);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var CameraService = (function () {
    function CameraService(camera, transfer, file, filePath, actionSheetCtrl, toastCtrl, platform, loadingCtrl) {
        this.camera = camera;
        this.transfer = transfer;
        this.file = file;
        this.filePath = filePath;
        this.actionSheetCtrl = actionSheetCtrl;
        this.toastCtrl = toastCtrl;
        this.platform = platform;
        this.loadingCtrl = loadingCtrl;
        this.lastImage = null;
    }
    CameraService.prototype.presentActionSheet = function (Car, returnFunction) {
        var _this = this;
        this.Car = Car;
        this.returnFunction = returnFunction;
        var actionSheet = this.actionSheetCtrl.create({
            title: 'Select Image Source',
            buttons: [
                {
                    text: 'Load from Library',
                    handler: function () {
                        _this.takePicture(_this.camera.PictureSourceType.PHOTOLIBRARY);
                    }
                },
                {
                    text: 'Use Camera',
                    handler: function () {
                        _this.takePicture(_this.camera.PictureSourceType.CAMERA);
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel'
                }
            ]
        });
        actionSheet.present();
    };
    CameraService.prototype.takePicture = function (sourceType) {
        var _this = this;
        // Create options for the Camera Dialog
        var options = {
            quality: 60,
            sourceType: sourceType,
            saveToPhotoAlbum: false,
            correctOrientation: true,
            targetWidth: 1000,
            targetHeight: 1000,
            allowEdit: true
        };
        // Get the data of an image
        this.camera.getPicture(options).then(function (imagePath) {
            // Special handling for Android library
            console.log("f0");
            if (_this.platform.is('android') && sourceType === _this.camera.PictureSourceType.PHOTOLIBRARY) {
                _this.filePath.resolveNativePath(imagePath)
                    .then(function (filePath) {
                    console.log("f01");
                    var correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                    console.log("f02");
                    var currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                    console.log("f03");
                    _this.copyFileToLocalDir(correctPath, currentName, _this.createFileName());
                });
            }
            else {
                var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
                var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
                _this.copyFileToLocalDir(correctPath, currentName, _this.createFileName());
            }
        }, function (err) {
            _this.presentToast('Error while selecting image.');
        });
    };
    // Create a new name for the image
    CameraService.prototype.createFileName = function () {
        console.log("f1");
        var d = new Date(), n = d.getTime(), newFileName = n + ".jpg";
        return newFileName;
    };
    // Copy the image to a local folder
    CameraService.prototype.copyFileToLocalDir = function (namePath, currentName, newFileName) {
        var _this = this;
        console.log("f2");
        this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(function (success) {
            _this.lastImage = newFileName;
            _this.returnFunction();
        }, function (error) {
            _this.presentToast('Error while storing file.');
        });
    };
    CameraService.prototype.presentToast = function (text) {
        console.log("f3");
        var toast = this.toastCtrl.create({
            message: text,
            duration: 3000,
            position: 'top'
        });
        toast.present();
    };
    // Always get the accurate path to your apps folder
    CameraService.prototype.pathForImage = function (img) {
        console.log("f4");
        if (img === null) {
            return '';
        }
        else {
            return cordova.file.dataDirectory + img;
        }
    };
    return CameraService;
}());
CameraService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_transfer__["a" /* Transfer */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_file__["a" /* File */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_file_path__["a" /* FilePath */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* ActionSheetController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["m" /* ToastController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* Platform */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* LoadingController */]])
], CameraService);

;
//# sourceMappingURL=camera.service.js.map

/***/ }),

/***/ 27:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Config; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var UserId = "http://tapper.co.il/647/laravel/public/api/";
var Config = (function () {
    function Config() {
        this.UserId = 3;
        this.ServerHost = "http://tapper.co.il/dagim/php/";
        this.defaultLanguage = 'he';
    }
    ;
    Config.prototype.updateLanguage = function (lang) {
        this.defaultLanguage = lang;
    };
    return Config;
}());
Config = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [])
], Config);

;
//# sourceMappingURL=config.js.map

/***/ }),

/***/ 296:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_send_to_server__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_basket_service__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_config__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the AboutPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var AboutPage = (function () {
    function AboutPage(navCtrl, navParams, Server, domSanitizer, BasketService, Settings) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.Server = Server;
        this.domSanitizer = domSanitizer;
        this.BasketService = BasketService;
        this.Settings = Settings;
        this.ServerUrl = "http://www.tapper.co.il/dagim/php/";
        this.BasketPrice = 0;
        this.defaultLangage = '';
        this.AboutObject = this.Server.AboutArray[0];
        this.AboutContent = this.AboutObject.content;
        this.AboutContentEnglish = this.AboutObject.content_english;
        this.MainImg = this.ServerUrl + '' + this.AboutObject.image;
        this.videoUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(this.AboutObject.video);
        console.log("AboutArray : ", this.MainImg);
        this.defaultLangage = this.Settings.defaultLanguage;
    }
    AboutPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AboutPage');
        this.CalculateBasket();
    };
    AboutPage.prototype.ngOnInit = function () {
        this.CalculateBasket();
    };
    AboutPage.prototype.CalculateBasket = function () {
        this.BasketPrice = this.BasketService.BasketTotalPrice;
        console.log("BasketPrice About", this.BasketPrice);
    };
    return AboutPage;
}());
AboutPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-about',template:/*ion-inline-start:"C:\Users\USER\Documents\github\salecar\salecar\src\pages\about\about.html"*/'    <ion-header>\n\n        <header></header>\n\n    </ion-header>\n\n\n\n\n\n\n\n\n\n    <ion-content>\n\n        <div class="AboutTitle">{{\'aboutustitle\' | translate}}</div>\n\n        <div *ngIf="defaultLangage == \'he\'" class="AboutDescription" [innerHTML]="AboutContent"></div>\n\n        <div *ngIf="defaultLangage == \'en\'" class="AboutDescription" [innerHTML]="AboutContentEnglish"></div>\n\n\n\n        <img src="{{MainImg}}" style="width: 100%" />\n\n\n\n        <div>\n\n            <iframe width="100%" height="200" [src]="videoUrl" frameborder="0" allowfullscreen></iframe>\n\n        </div>\n\n\n\n    </ion-content>\n\n\n\n    <ion-footer class="FooterClass">\n\n        <footer></footer>\n\n    </ion-footer>\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\salecar\salecar\src\pages\about\about.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_send_to_server__["a" /* SendToServerService */], __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__["c" /* DomSanitizer */], __WEBPACK_IMPORTED_MODULE_4__services_basket_service__["a" /* BasketService */], __WEBPACK_IMPORTED_MODULE_5__services_config__["a" /* Config */]])
], AboutPage);

//# sourceMappingURL=about.js.map

/***/ }),

/***/ 297:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_config__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_send_to_server__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_popups__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ng2_translate__ = __webpack_require__(48);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the ContactPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var ContactPage = (function () {
    function ContactPage(navCtrl, navParams, alertCtrl, Settings, Server, Popup, translate) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.Settings = Settings;
        this.Server = Server;
        this.Popup = Popup;
        this.translate = translate;
        this.Contact = {
            'name': '',
            'mail': '',
            'details': '',
            'phone': '',
        };
        translate.onLangChange.subscribe(function (event) {
        });
    }
    ContactPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ContactPage');
    };
    ContactPage.prototype.SendForm = function () {
        if (this.Contact.name.length < 3)
            this.Popup.presentAlert(this.translate.instant("missingFieldsText"), this.translate.instant("requiredFullnameText"), 0);
        else if (this.Contact.mail.length < 3)
            this.Popup.presentAlert(this.translate.instant("missingFieldsText"), this.translate.instant("requiredEmailText"), 0);
        else if (this.Contact.phone.length < 3)
            this.Popup.presentAlert(this.translate.instant("missingFieldsText"), this.translate.instant("requiredPhonetext"), 0);
        else if (this.Contact.details.length < 3)
            this.Popup.presentAlert(this.translate.instant("missingFieldsText"), this.translate.instant("requiredDescText"), 0);
        else {
            this.Settings.ContactDetails = this.Contact;
            this.Server.sendContactDetails('getContact');
            this.Popup.presentAlert(this.translate.instant("sentOkTitleText"), this.translate.instant("SendOktextText"), 1, this.RedirectFun());
        }
    };
    ContactPage.prototype.RedirectFun = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
    };
    return ContactPage;
}());
ContactPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-contact',template:/*ion-inline-start:"C:\Users\USER\Documents\github\salecar\salecar\src\pages\contact\contact.html"*/'<!--\n\n  Generated template for the RegisterPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n  <header></header>\n\n</ion-header>\n\n\n\n\n\n\n\n<ion-content padding>\n\n  <ion-item no-lines class="RegTitle">\n\n    <label>{{\'contacttitletext\' | translate}}</label>\n\n  </ion-item>\n\n\n\n  <ion-list class="Inputs">\n\n    <ion-item>\n\n      <ion-label floating>{{\'fullnametext\' | translate}}</ion-label>\n\n      <ion-input type="text" [(ngModel)]="Contact.name" name="name" ></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label floating>{{\'phonetext\' | translate}}</ion-label>\n\n      <ion-input type="tel" [(ngModel)]="Contact.phone" name="phone" ></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label floating>{{\'emailtext\' | translate}}</ion-label>\n\n      <ion-input type="text" [(ngModel)]="Contact.mail" name="mail" ></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label floating>{{\'desctext\' | translate}}</ion-label>\n\n      <ion-textarea type="text" [(ngModel)]="Contact.details" name="details" ></ion-textarea>\n\n    </ion-item>\n\n\n\n\n\n    <div style="width: 100%; margin-top: 20px;" align="center" (click)="SendForm()">\n\n      <div style="width: 90%">\n\n        <button ion-button color="primary" style="width: 100%">{{\'sendtext\' | translate}}</button>\n\n      </div>\n\n    </div>\n\n\n\n\n\n\n\n  </ion-list>\n\n</ion-content>\n\n<ion-footer class="FooterClass">\n\n  <footer></footer>\n\n</ion-footer>'/*ion-inline-end:"C:\Users\USER\Documents\github\salecar\salecar\src\pages\contact\contact.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_3__services_config__["a" /* Config */], __WEBPACK_IMPORTED_MODULE_4__services_send_to_server__["a" /* SendToServerService */], __WEBPACK_IMPORTED_MODULE_5__services_popups__["a" /* PopupService */], __WEBPACK_IMPORTED_MODULE_6_ng2_translate__["c" /* TranslateService */]])
], ContactPage);

//# sourceMappingURL=contact.js.map

/***/ }),

/***/ 298:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SubcategoryPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_send_to_server__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_config__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__shop_shop__ = __webpack_require__(163);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the SubcategoryPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var SubcategoryPage = (function () {
    function SubcategoryPage(navCtrl, navParams, Server, Settings) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.Server = Server;
        this.Settings = Settings;
        this.ServerHost = this.Settings.ServerHost;
        this.CategoryId = navParams.get('id');
        this.SubCategoryArray = this.Server.ProductsArray[this.CategoryId].subcat;
        console.log("categories: ", this.SubCategoryArray);
    }
    SubcategoryPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SubcategoryPage');
    };
    SubcategoryPage.prototype.goToShopPage = function (index, item) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__shop_shop__["a" /* ShopPage */], { cat: this.CategoryId, subcat: index });
    };
    return SubcategoryPage;
}());
SubcategoryPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-subcategory',template:/*ion-inline-start:"C:\Users\USER\Documents\github\salecar\salecar\src\pages\subcategory\subcategory.html"*/'<ion-header>\n\n  <header></header>\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <div class="Product" *ngFor="let item of SubCategoryArray let i=index" (click)="goToShopPage(i,item)">\n\n    <img src="{{ServerHost+item.image}}" style="width:100%;">\n\n  </div>\n\n</ion-content>\n\n\n\n\n\n<ion-footer class="FooterClass">\n\n  <footer></footer>\n\n</ion-footer>\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\salecar\salecar\src\pages\subcategory\subcategory.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_send_to_server__["a" /* SendToServerService */], __WEBPACK_IMPORTED_MODULE_3__services_config__["a" /* Config */]])
], SubcategoryPage);

//# sourceMappingURL=subcategory.js.map

/***/ }),

/***/ 299:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(300);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(304);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 304:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(293);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(294);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__(606);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_home_home__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_about_about__ = __webpack_require__(296);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__services_send_to_server__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_http__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__services_config__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_shop_shop__ = __webpack_require__(163);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_single_single__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_basket_basket__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__services_basket_service__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_register_register__ = __webpack_require__(153);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__components_footer_footer__ = __webpack_require__(607);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__components_header_header__ = __webpack_require__(608);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_contact_contact__ = __webpack_require__(297);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__ionic_native_social_sharing__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__ionic_native_in_app_browser__ = __webpack_require__(295);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_popup_popup__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_categories_categories__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_login_login__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_forgot_forgot__ = __webpack_require__(152);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__services_popups__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__services_user__ = __webpack_require__(609);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27_ng2_translate__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__pages_subcategory_subcategory__ = __webpack_require__(298);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__pages_companies_companies__ = __webpack_require__(154);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__services_CompanyService__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__pages_car_car__ = __webpack_require__(155);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__pages_company_admin_company_admin__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__pages_add_car_add_car__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__pages_edit_car_edit_car__ = __webpack_require__(157);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__pages_image_car_image_car__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__ionic_native_camera__ = __webpack_require__(130);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__ionic_native_diagnostic__ = __webpack_require__(251);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__ionic_native_file_transfer__ = __webpack_require__(252);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__services_camera_service__ = __webpack_require__(253);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__ionic_native_file_path__ = __webpack_require__(129);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__ionic_native_transfer__ = __webpack_require__(128);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__ionic_native_file__ = __webpack_require__(127);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__pages_manager_manager__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44__pages_edit_company_edit_company__ = __webpack_require__(158);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_45__pages_add_company_add_company__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_46__pages_sub_categories_sub_categories__ = __webpack_require__(161);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_47__pages_single_car_single_car__ = __webpack_require__(162);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
















































var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_7__pages_about_about__["a" /* AboutPage */],
            __WEBPACK_IMPORTED_MODULE_11__pages_shop_shop__["a" /* ShopPage */], __WEBPACK_IMPORTED_MODULE_12__pages_single_single__["a" /* SinglePage */], __WEBPACK_IMPORTED_MODULE_13__pages_basket_basket__["a" /* BasketPage */],
            __WEBPACK_IMPORTED_MODULE_15__pages_register_register__["a" /* RegisterPage */],
            __WEBPACK_IMPORTED_MODULE_16__components_footer_footer__["a" /* FooterComponent */],
            __WEBPACK_IMPORTED_MODULE_17__components_header_header__["a" /* HeaderComponent */],
            __WEBPACK_IMPORTED_MODULE_18__pages_contact_contact__["a" /* ContactPage */],
            __WEBPACK_IMPORTED_MODULE_21__pages_popup_popup__["a" /* PopupPage */],
            __WEBPACK_IMPORTED_MODULE_22__pages_categories_categories__["a" /* CategoriesPage */],
            __WEBPACK_IMPORTED_MODULE_23__pages_login_login__["a" /* LoginPage */],
            __WEBPACK_IMPORTED_MODULE_24__pages_forgot_forgot__["a" /* ForgotPage */],
            __WEBPACK_IMPORTED_MODULE_28__pages_subcategory_subcategory__["a" /* SubcategoryPage */],
            __WEBPACK_IMPORTED_MODULE_29__pages_companies_companies__["a" /* CompaniesPage */],
            __WEBPACK_IMPORTED_MODULE_31__pages_car_car__["a" /* CarPage */],
            __WEBPACK_IMPORTED_MODULE_32__pages_company_admin_company_admin__["a" /* CompanyAdminPage */],
            __WEBPACK_IMPORTED_MODULE_33__pages_add_car_add_car__["a" /* AddCarPage */],
            __WEBPACK_IMPORTED_MODULE_34__pages_edit_car_edit_car__["a" /* EditCarPage */],
            __WEBPACK_IMPORTED_MODULE_35__pages_image_car_image_car__["a" /* ImageCarPage */],
            __WEBPACK_IMPORTED_MODULE_43__pages_manager_manager__["a" /* ManagerPage */],
            __WEBPACK_IMPORTED_MODULE_44__pages_edit_company_edit_company__["a" /* EditCompanyPage */],
            __WEBPACK_IMPORTED_MODULE_45__pages_add_company_add_company__["a" /* AddCompanyPage */],
            __WEBPACK_IMPORTED_MODULE_46__pages_sub_categories_sub_categories__["a" /* SubCategoriesPage */],
            __WEBPACK_IMPORTED_MODULE_47__pages_single_car_single_car__["a" /* SingleCarPage */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */], {}, {
                links: [
                    { loadChildren: '../pages/popup/popup.module#PopupPageModule', name: 'PopupPage', segment: 'popup', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/forgot/forgot.module#ForgotPageModule', name: 'ForgotPage', segment: 'forgot', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/image-car/image-car.module#ImageCarPageModule', name: 'ImageCarPage', segment: 'image-car', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/add-car/add-car.module#AddCarPageModule', name: 'AddCarPage', segment: 'add-car', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/edit-car/edit-car.module#EditCarPageModule', name: 'EditCarPage', segment: 'edit-car', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/company-admin/company-admin.module#CompanyAdminPageModule', name: 'CompanyAdminPage', segment: 'company-admin', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/edit-company/edit-company.module#EditCompanyPageModule', name: 'EditCompanyPage', segment: 'edit-company', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/add-company/add-company.module#AddCompanyPageModule', name: 'AddCompanyPage', segment: 'add-company', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/sub-categories/sub-categories.module#SubCategoriesPageModule', name: 'SubCategoriesPage', segment: 'sub-categories', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/categories/categories.module#CategoriesPageModule', name: 'CategoriesPage', segment: 'categories', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/manager/manager.module#ManagerPageModule', name: 'ManagerPage', segment: 'manager', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/basket/basket.module#BasketPageModule', name: 'BasketPage', segment: 'basket', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/register/register.module#RegisterPageModule', name: 'RegisterPage', segment: 'register', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/car/car.module#CarPageModule', name: 'CarPage', segment: 'car', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/single/single.module#SinglePageModule', name: 'SinglePage', segment: 'single', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/companies/companies.module#CompaniesPageModule', name: 'CompaniesPage', segment: 'companies', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/single-car/single-car.module#SingleCarPageModule', name: 'SingleCarPage', segment: 'single-car', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/about/about.module#AboutPageModule', name: 'AboutPage', segment: 'about', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/shop/shop.module#ShopPageModule', name: 'ShopPage', segment: 'shop', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/contact/contact.module#ContactPageModule', name: 'ContactPage', segment: 'contact', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/subcategory/subcategory.module#SubcategoryPageModule', name: 'SubcategoryPage', segment: 'subcategory', priority: 'low', defaultHistory: [] }
                ]
            }),
            __WEBPACK_IMPORTED_MODULE_9__angular_http__["c" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_27_ng2_translate__["b" /* TranslateModule */].forRoot({
                provide: __WEBPACK_IMPORTED_MODULE_27_ng2_translate__["a" /* TranslateLoader */],
                useFactory: function (http) { return new __WEBPACK_IMPORTED_MODULE_27_ng2_translate__["d" /* TranslateStaticLoader */](http, '/assets/i18n', '.json'); },
                deps: [__WEBPACK_IMPORTED_MODULE_9__angular_http__["b" /* Http */]]
            })
        ],
        exports: [__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */], __WEBPACK_IMPORTED_MODULE_9__angular_http__["c" /* HttpModule */], __WEBPACK_IMPORTED_MODULE_27_ng2_translate__["b" /* TranslateModule */]],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicApp */]],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_7__pages_about_about__["a" /* AboutPage */],
            __WEBPACK_IMPORTED_MODULE_11__pages_shop_shop__["a" /* ShopPage */],
            __WEBPACK_IMPORTED_MODULE_12__pages_single_single__["a" /* SinglePage */],
            __WEBPACK_IMPORTED_MODULE_13__pages_basket_basket__["a" /* BasketPage */],
            __WEBPACK_IMPORTED_MODULE_15__pages_register_register__["a" /* RegisterPage */],
            __WEBPACK_IMPORTED_MODULE_16__components_footer_footer__["a" /* FooterComponent */],
            __WEBPACK_IMPORTED_MODULE_17__components_header_header__["a" /* HeaderComponent */],
            __WEBPACK_IMPORTED_MODULE_18__pages_contact_contact__["a" /* ContactPage */],
            __WEBPACK_IMPORTED_MODULE_21__pages_popup_popup__["a" /* PopupPage */],
            __WEBPACK_IMPORTED_MODULE_22__pages_categories_categories__["a" /* CategoriesPage */],
            __WEBPACK_IMPORTED_MODULE_23__pages_login_login__["a" /* LoginPage */],
            __WEBPACK_IMPORTED_MODULE_24__pages_forgot_forgot__["a" /* ForgotPage */],
            __WEBPACK_IMPORTED_MODULE_28__pages_subcategory_subcategory__["a" /* SubcategoryPage */],
            __WEBPACK_IMPORTED_MODULE_29__pages_companies_companies__["a" /* CompaniesPage */],
            __WEBPACK_IMPORTED_MODULE_31__pages_car_car__["a" /* CarPage */],
            __WEBPACK_IMPORTED_MODULE_32__pages_company_admin_company_admin__["a" /* CompanyAdminPage */],
            __WEBPACK_IMPORTED_MODULE_33__pages_add_car_add_car__["a" /* AddCarPage */],
            __WEBPACK_IMPORTED_MODULE_34__pages_edit_car_edit_car__["a" /* EditCarPage */],
            __WEBPACK_IMPORTED_MODULE_35__pages_image_car_image_car__["a" /* ImageCarPage */],
            __WEBPACK_IMPORTED_MODULE_43__pages_manager_manager__["a" /* ManagerPage */],
            __WEBPACK_IMPORTED_MODULE_44__pages_edit_company_edit_company__["a" /* EditCompanyPage */],
            __WEBPACK_IMPORTED_MODULE_45__pages_add_company_add_company__["a" /* AddCompanyPage */],
            __WEBPACK_IMPORTED_MODULE_46__pages_sub_categories_sub_categories__["a" /* SubCategoriesPage */],
            __WEBPACK_IMPORTED_MODULE_47__pages_single_car_single_car__["a" /* SingleCarPage */]
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["v" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicErrorHandler */] },
            __WEBPACK_IMPORTED_MODULE_8__services_send_to_server__["a" /* SendToServerService */],
            __WEBPACK_IMPORTED_MODULE_10__services_config__["a" /* Config */],
            __WEBPACK_IMPORTED_MODULE_14__services_basket_service__["a" /* BasketService */],
            __WEBPACK_IMPORTED_MODULE_19__ionic_native_social_sharing__["a" /* SocialSharing */],
            __WEBPACK_IMPORTED_MODULE_20__ionic_native_in_app_browser__["a" /* InAppBrowser */],
            __WEBPACK_IMPORTED_MODULE_25__services_popups__["a" /* PopupService */],
            __WEBPACK_IMPORTED_MODULE_26__services_user__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_30__services_CompanyService__["a" /* CompanyService */],
            __WEBPACK_IMPORTED_MODULE_41__ionic_native_transfer__["a" /* Transfer */],
            __WEBPACK_IMPORTED_MODULE_40__ionic_native_file_path__["a" /* FilePath */],
            __WEBPACK_IMPORTED_MODULE_39__services_camera_service__["a" /* CameraService */],
            __WEBPACK_IMPORTED_MODULE_36__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_37__ionic_native_diagnostic__["a" /* Diagnostic */],
            __WEBPACK_IMPORTED_MODULE_38__ionic_native_file_transfer__["a" /* FileTransfer */],
            __WEBPACK_IMPORTED_MODULE_42__ionic_native_file__["a" /* File */]
        ]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 37:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PopupService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PopupService = (function () {
    function PopupService(alertCtrl) {
        this.alertCtrl = alertCtrl;
    }
    ;
    PopupService.prototype.presentAlert = function (Title, Message, PageRedirect, fun) {
        if (fun === void 0) { fun = null; }
        var alert = this.alertCtrl.create({
            title: Title,
            subTitle: Message,
            buttons: [{
                    text: 'סגור',
                    handler: function () { if (PageRedirect == 1) {
                        fun;
                    } }
                }],
            cssClass: 'alertRtl'
        });
        alert.present();
    };
    return PopupService;
}());
PopupService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* AlertController */]])
], PopupService);

//# sourceMappingURL=popups.js.map

/***/ }),

/***/ 49:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_send_to_server__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__register_register__ = __webpack_require__(153);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__forgot_forgot__ = __webpack_require__(152);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_popups__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ng2_translate__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__company_admin_company_admin__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__manager_manager__ = __webpack_require__(54);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var LoginPage = (function () {
    function LoginPage(navCtrl, navParams, alertCtrl, Server, Popup, translate) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.Server = Server;
        this.Popup = Popup;
        this.translate = translate;
        this.Login = {
            "mail": "",
            "password": ""
        };
        translate.setDefaultLang('he');
        translate.onLangChange.subscribe(function (event) {
        });
    }
    LoginPage.prototype.loginUser = function () {
        var _this = this;
        this.emailregex = /\S+@\S+\.\S+/;
        if (this.Login.mail == "")
            this.Popup.presentAlert(this.translate.instant("requiredtext"), this.translate.instant("inputmailtext"), '');
        else if (!this.emailregex.test(this.Login.mail)) {
            this.Popup.presentAlert(this.translate.instant("requiredtext"), this.translate.instant("requiredEmailText"), '');
            this.Login.mail = '';
        }
        else if (this.Login.password == "")
            this.Popup.presentAlert(this.translate.instant("requiredtext"), this.translate.instant("missingpasswordtext"), '');
        else {
            this.serverResponse = this.Server.LoginUser("UserLogin", this.Login).then(function (data) {
                console.log("login response: ", data);
                if (data == 0) {
                    _this.Popup.presentAlert(_this.translate.instant("badlogintitle"), _this.translate.instant("bademailorpasswordtext"), '');
                    _this.Login.password = '';
                }
                else {
                    window.localStorage.userid = data['id'];
                    window.localStorage.name = data['name'];
                    window.localStorage.isAdmin = data['isadmin'];
                    _this.Login.mail = '';
                    _this.Login.password = '';
                    if (data['isadmin'] == 0)
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__company_admin_company_admin__["a" /* CompanyAdminPage */]);
                    else
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__manager_manager__["a" /* ManagerPage */]);
                }
            });
        }
    };
    LoginPage.prototype.goRegisterPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__register_register__["a" /* RegisterPage */]);
    };
    LoginPage.prototype.goForgotPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__forgot_forgot__["a" /* ForgotPage */]);
    };
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
    };
    return LoginPage;
}());
LoginPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-login',template:/*ion-inline-start:"C:\Users\USER\Documents\github\salecar\salecar\src\pages\login\login.html"*/'<!--\n\n  Generated template for the RegisterPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n  <header></header>\n\n</ion-header>\n\n\n\n\n\n\n\n<ion-content padding>\n\n  <ion-item no-lines class="RegTitle">\n\n    <label>התחברות</label>\n\n  </ion-item>\n\n\n\n  <ion-list class="Inputs">\n\n\n\n    <ion-item>\n\n      <ion-label floating>הכנס מייל</ion-label>\n\n      <ion-input type="mail" [(ngModel)]="Login.mail" name="mail" ></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n      <ion-label floating>הכנס סיסמה</ion-label>\n\n      <ion-input type="password" [(ngModel)]="Login.password" name="password" ></ion-input>\n\n    </ion-item>\n\n\n\n\n\n    <div style="width: 100%; margin-top: 20px;" align="center" (click)="loginUser()">\n\n      <div style="width: 90%">\n\n        <button ion-button color="primary" style="width: 100%; color:black">שלח</button>\n\n      </div>\n\n    </div>\n\n\n\n\n\n\n\n\n\n  </ion-list>\n\n</ion-content>\n\n<ion-footer class="FooterClass">\n\n  <footer></footer>\n\n</ion-footer>'/*ion-inline-end:"C:\Users\USER\Documents\github\salecar\salecar\src\pages\login\login.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_2__services_send_to_server__["a" /* SendToServerService */], __WEBPACK_IMPORTED_MODULE_5__services_popups__["a" /* PopupService */], __WEBPACK_IMPORTED_MODULE_6_ng2_translate__["c" /* TranslateService */]])
], LoginPage);

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 51:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BasketService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Subject__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Subject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_Subject__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var BasketService = (function () {
    function BasketService(http, Settings) {
        this.http = http;
        this.Settings = Settings;
        this.basket = [];
        this.Obj = new Object();
        this.BasketTotalPrice = 0;
        this.productFound = 0;
        this._BasketTotalPrice = new __WEBPACK_IMPORTED_MODULE_4_rxjs_Subject__["Subject"]();
        this.BasketTotalPrice$ = this._BasketTotalPrice.asObservable();
    }
    ;
    BasketService.prototype.AddProductsProducts = function (productId, Qnt, price, name, nameenglish, image) {
        this.productFound = 0;
        if (this.basket.length > 0) {
            for (var i = 0; i < this.basket.length; i++) {
                if (this.basket[i].productId == productId) {
                    this.productFound = 1;
                    this.basket[i].Qnt += Number(Qnt);
                    //this.basket[i].Pay +=  Number(Pay);
                }
            }
        }
        if (this.productFound == 0) {
            this.Obj = {
                "productId": productId,
                "Qnt": Number(Qnt),
                //"Pay":Number(Pay),
                "price": price,
                "name": name,
                "name_english": nameenglish,
                "image": image
            };
            this.basket.push(this.Obj);
        }
        this.CalculateBasket();
        console.log("Basket : ", this.basket);
    };
    BasketService.prototype.CalculateBasket = function () {
        this.BasketTotalPrice = 0;
        for (var i = 0; i < this.basket.length; i++) {
            this.BasketTotalPrice += parseInt(this.basket[i].price) * this.basket[i].Qnt;
            console.log("CalculateBasket", this.BasketTotalPrice);
        }
        this.setPrice(this.BasketTotalPrice);
    };
    BasketService.prototype.setPrice = function (value) {
        this.BasketTotalPrice = value;
        this._BasketTotalPrice.next(value);
    };
    BasketService.prototype.resetTotalPrice = function () {
        this.BasketTotalPrice = 0;
    };
    BasketService.prototype.emptyBasket = function () {
        this.basket = [];
    };
    BasketService.prototype.deleteProduct = function (index) {
        this.basket.splice(index, 1);
        this.CalculateBasket();
    };
    return BasketService;
}());
BasketService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_3__config__["a" /* Config */]])
], BasketService);

;
//# sourceMappingURL=basket.service.js.map

/***/ }),

/***/ 52:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PopupPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the PopupPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var PopupPage = (function () {
    function PopupPage(navCtrl, navParams, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.img_url = this.navParams.get('url');
    }
    PopupPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PopupPage');
    };
    PopupPage.prototype.closeModal = function () {
        this.viewCtrl.dismiss();
    };
    return PopupPage;
}());
PopupPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-popup',template:/*ion-inline-start:"C:\Users\USER\Documents\github\salecar\salecar\src\pages\popup\popup.html"*/'<ion-content>\n\n\n\n  <ion-row>\n\n    <ion-col text-right padding-right>\n\n      <ion-icon name="close" style="font-size: 30px;" (click)="closeModal()"></ion-icon>\n\n    </ion-col>\n\n  </ion-row>\n\n\n\n  <div class="div90" text-center>\n\n    <img src="{{img_url}}" class="w100" />\n\n  </div>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\salecar\salecar\src\pages\popup\popup.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */]])
], PopupPage);

//# sourceMappingURL=popup.js.map

/***/ }),

/***/ 53:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CompanyAdminPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_CompanyService__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__add_car_add_car__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__edit_car_edit_car__ = __webpack_require__(157);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__login_login__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_social_sharing__ = __webpack_require__(88);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the CompanyAdminPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var CompanyAdminPage = (function () {
    function CompanyAdminPage(navCtrl, socialSharing, companyService, navParams, alertCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.socialSharing = socialSharing;
        this.companyService = companyService;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.Company = [];
        this.Cars = [];
        this.CompanyId = '';
        this.CompanyPlace = '';
        // public host = "http://www.tapper.co.il/salecar/admin/public"
        this.host = "http://www.tapper.co.il/salecar/laravel/storage/app/public/";
        this.message = 'הזמין אותך לצפות בכרטיס ביקור המכיל את כל הרכבים שלו לצפייה לחץ כאן :';
        this.link = '';
        this.fullMessage = '';
        this.fullImage = '';
        this.smsMessage = '';
        console.log("shay");
        this.companyService.GetCompanyById('GetCompanyById').then(function (data) {
            console.log("CarAdmin : ", data), _this.Company = data;
            _this.Cars = _this.Company['cars'];
            _this.link = "http://" + _this.Company['subdomain'] + ".salecar.co.il";
            _this.fullMessage = _this.Company['name'] + _this.message;
            _this.fullImage = _this.host + "/" + _this.Company['logo'];
            _this.smsMessage = _this.fullMessage + " " + _this.link;
        });
    }
    CompanyAdminPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CompanyAdminPage');
    };
    CompanyAdminPage.prototype.AddCar = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__add_car_add_car__["a" /* AddCarPage */], { CompanyId: this.Company['id'] });
    };
    CompanyAdminPage.prototype.EditCar = function (Id) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__edit_car_edit_car__["a" /* EditCarPage */], { CarId: Id });
    };
    CompanyAdminPage.prototype.LogOut = function () {
        window.localStorage.userid = '';
        window.localStorage.name = '';
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__login_login__["a" /* LoginPage */]);
    };
    CompanyAdminPage.prototype.openPopUp = function () {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'שלח כרטיס לחבר',
            message: "על מנת לשלוח את הכרטיס ללקוח שלך הכנס מספר טלפון והוא יישלח מיידית",
            cssClass: 'alertClass',
            inputs: [
                {
                    name: 'phone',
                    placeholder: 'הכנס מספר טלפון'
                },
            ],
            buttons: [
                {
                    text: 'בטל',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'שלח',
                    handler: function (data) {
                        var phone = data.phone;
                        /*while(phone.charAt(0) === '0')
                        {
                            phone= "+972"+phone.substr(1);
                        }*/
                        _this.companyService.SendSms('SendSms', _this.smsMessage, data.phone).then(function (data) {
                            console.log('SendSms');
                        });
                        console.log('Saved clicked : ', phone);
                    }
                }
            ]
        });
        prompt.present();
    };
    CompanyAdminPage.prototype.whatsApp = function () {
        this.socialSharing.shareViaWhatsApp(this.fullMessage, this.fullImage, this.link).then(function () {
            // Success!
        }).catch(function () {
            // Error!
        });
    };
    return CompanyAdminPage;
}());
CompanyAdminPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-company-admin',template:/*ion-inline-start:"C:\Users\USER\Documents\github\salecar\salecar\src\pages\company-admin\company-admin.html"*/'<!-- <ion-header>\n\n  <header></header>\n\n</ion-header> -->\n\n\n\n<ion-toolbar no-border-top class="ToolBarClass1">\n\n\n\n\n\n  <ion-buttons left>\n\n    <button navPop style="background-color: black">\n\n      <ion-icon style="color: white; font-size: 30px;" name="ios-arrow-back-outline"></ion-icon>\n\n    </button>\n\n  </ion-buttons>\n\n\n\n  <div class="ToolBarTitle">\n\n    <ion-title>\n\n      <div>\n\n        <img src="images/logo1.png" style="width: 130px; margin-top: 4px;">\n\n      </div>\n\n    </ion-title>\n\n  </div>\n\n\n\n  <ion-buttons right>\n\n    <button (click)="LogOut()" style="background-color: black">\n\n      <ion-icon style="color: white; font-size: 40px; margin-right:20px;" name="ios-exit-outline"></ion-icon>\n\n      <p style="color: white; font-size: 10px; margin-top: -7px;">התנתק</p>\n\n    </button>\n\n  </ion-buttons>\n\n</ion-toolbar>\n\n\n\n\n\n\n\n<ion-content class="Content" style="margin-top:75px;">\n\n\n\n\n\n  <div class="Info">\n\n    <div class="Logo" align="center">\n\n      <div class="w80">\n\n        <img *ngIf="Company.logo != null" src="{{host}}/{{Company.logo}}" style="width:100%; margin-top:-3px;"  />\n\n        <img *ngIf="Company.logo == null" src="images/logos.png"/>\n\n      </div>\n\n    </div>\n\n    <div class="Details">\n\n      <p>{{Company.name}}</p>\n\n      <h3>{{Company.address}}</h3>\n\n      <h4>טלפון : {{Company.phone}}</h4>\n\n    </div>\n\n    <div class="plus" (click)="AddCar()">\n\n      <div class="w80">\n\n        <img  src="images/plus.png" style="width:100%" />\n\n        <p>הוסף רכב</p>\n\n      </div>\n\n    </div>\n\n  </div>\n\n  <div style="width:100%" align="center">\n\n    <div class="c3Line">\n\n    </div>\n\n  </div>\n\n  <!-- Cars -->\n\n  <div>\n\n    <div *ngFor="let car of Cars let i = index"  >\n\n      <div class="ListRow" (click)="EditCar(i)">\n\n        <div class="ListRight" *ngIf="car.images.length > 0">\n\n          <img  src="{{host}}{{car.images[0].url}}" style="width:100%; margin-top:-3px;"  />\n\n        </div>\n\n          <div class="ListRight" *ngIf="car.images.length == 0">\n\n              <img  src="{{host}}{{Company.logo}}" style="width:100%; margin-top:-3px;"  />\n\n          </div>\n\n        <div class="ListCenter"  >\n\n          <p class="bName">{{car.carName}}</p>\n\n          <h3> {{car.carModelName}}</h3>\n\n          <h4 style="direction: rtl">שנה : {{car.year}} </h4>\n\n        </div>\n\n\n\n        <div class="ListLeft">\n\n          <div>\n\n            <img  src="images/arrow.png" style="width:20px;" />\n\n          </div>\n\n        </div>\n\n      </div>\n\n\n\n      <div>\n\n        <img  src="images/line.png" style="width:100%; margin-top:-3px;" />\n\n      </div>\n\n    </div>\n\n  </div>\n\n</ion-content>\n\n<ion-footer >\n\n  <ion-toolbar>\n\n    <ion-buttons  style="!important; text-align: center; padding: 0; margin: 0;"  >\n\n      <button ion-button icon-right color="royal" style=" font-size: 17px; font-weight: bold;  color: white;" (click)="openPopUp()">\n\n      SMS\n\n      </button>\n\n      <button ion-button icon-right color="royal" style=" font-size: 22px; font-weight: bold;  color: white;">\n\n        |\n\n      </button>\n\n      <button ion-button icon-right color="royal" style=" font-size: 17px; font-weight: bold;  color: white;" (click)="whatsApp()">\n\n       Whatsapp\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-toolbar>\n\n</ion-footer>'/*ion-inline-end:"C:\Users\USER\Documents\github\salecar\salecar\src\pages\company-admin\company-admin.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_social_sharing__["a" /* SocialSharing */], __WEBPACK_IMPORTED_MODULE_2__services_CompanyService__["a" /* CompanyService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
], CompanyAdminPage);

//# sourceMappingURL=company-admin.js.map

/***/ }),

/***/ 54:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ManagerPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_CompanyService__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__edit_company_edit_company__ = __webpack_require__(158);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__add_company_add_company__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__login_login__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__categories_categories__ = __webpack_require__(160);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the ManagerPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var ManagerPage = (function () {
    function ManagerPage(navCtrl, navParams, companyService) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.companyService = companyService;
        this.CompaniesArray = [];
        //public host = "http://www.tapper.co.il/salecar/admin/public"
        this.host = "http://www.tapper.co.il/salecar/laravel/storage/app/public/";
        this.companyService.GetAllCompanies('GetAllCompanies').then(function (data) {
            console.log("Carray : ", data), _this.CompaniesArray = data;
        });
    }
    ManagerPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ManagerPage');
    };
    ManagerPage.prototype.goToEditCompany = function (id) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__edit_company_edit_company__["a" /* EditCompanyPage */], { CompanyId: id });
    };
    ManagerPage.prototype.goToAddCompany = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__add_company_add_company__["a" /* AddCompanyPage */]);
    };
    ManagerPage.prototype.LogOut = function () {
        window.localStorage.userid = '';
        window.localStorage.name = '';
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__login_login__["a" /* LoginPage */]);
    };
    ManagerPage.prototype.GoToCategories = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__categories_categories__["a" /* CategoriesPage */]);
    };
    ;
    return ManagerPage;
}());
ManagerPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-manager',template:/*ion-inline-start:"C:\Users\USER\Documents\github\salecar\salecar\src\pages\manager\manager.html"*/'<ion-toolbar no-border-top class="ToolBarClass1">\n\n\n\n\n\n  <ion-buttons left>\n\n    <button navPop style="background-color: black">\n\n      <ion-icon style="color: white; font-size: 30px;" name="ios-arrow-back-outline"></ion-icon>\n\n    </button>\n\n  </ion-buttons>\n\n\n\n  <div class="ToolBarTitle">\n\n    <ion-title>\n\n      <div (click)="LogOut()">\n\n        <img src="images/logo1.png" style="width: 130px; margin-top: 4px;">\n\n      </div>\n\n    </ion-title>\n\n  </div>\n\n\n\n  <ion-buttons right>\n\n    <button  style="background-color: black" (click)="goToAddCompany()">\n\n      <ion-icon style="color: white; font-size: 40px; margin-right:20px;" name="ios-add-circle-outline"></ion-icon>\n\n      <p style="color: white; font-size: 10px; margin-top: 3px;">הוסף חברה</p>\n\n    </button>\n\n  </ion-buttons>\n\n</ion-toolbar>\n\n\n\n\n\n<ion-content padding class="Content">\n\n<div  style="padding-bottom: 200px">\n\n  <div *ngFor="let item of CompaniesArray let i = index" >\n\n    <div class="ListRow" (click)="goToEditCompany(i)">\n\n      <div class="ListRight"  >\n\n        <img *ngIf="item.logo != null"  src="{{host}}/{{item.logo}}" style="width:100%; margin-top:-3px;"  />\n\n        <img *ngIf="item.logo == null"  src="images/logos.png" style="width:100%; margin-top:-3px;"  />\n\n      </div>\n\n      <div class="ListCenter"  >\n\n        <p class="bName">{{item.name}}</p>\n\n        <h3>{{item.phone}}</h3>\n\n        <h4>{{item.address}}</h4>\n\n      </div>\n\n\n\n      <div class="ListLeft">\n\n        <div>\n\n          <img  src="images/arrow.png" style="width:20px;" />\n\n        </div>\n\n      </div>\n\n    </div>\n\n\n\n    <div>\n\n      <img  src="images/line.png" style="width:100%; margin-top:-3px;" />\n\n    </div>\n\n  </div>\n\n</div>\n\n</ion-content>\n\n\n\n\n\n<ion-footer >\n\n  <ion-toolbar>\n\n    <ion-buttons  style="!important; text-align: center; padding: 0; margin: 0;"  >\n\n      <button ion-button icon-right color="royal" style=" font-size: 17px; font-weight: bold;  color: white;" (click)="LogOut()">\n\n        התנתק\n\n      </button>\n\n      <button ion-button icon-right color="royal" style=" font-size: 22px; font-weight: bold;  color: white;">\n\n        |\n\n      </button>\n\n      <button ion-button icon-right color="royal" style=" font-size: 17px; font-weight: bold;  color: white;" (click)="GoToCategories()">\n\n        קטגוריות\n\n      </button>\n\n      <button ion-button icon-right color="royal" style=" font-size: 22px; font-weight: bold;  color: white;">\n\n        |\n\n      </button>\n\n      <button ion-button icon-right color="royal" style=" font-size: 17px; font-weight: bold;  color: white;" (click)="goToAddCompany()">\n\n        הוסף חברה\n\n      </button>\n\n\n\n\n\n    </ion-buttons>\n\n  </ion-toolbar>\n\n</ion-footer>'/*ion-inline-end:"C:\Users\USER\Documents\github\salecar\salecar\src\pages\manager\manager.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_CompanyService__["a" /* CompanyService */]])
], ManagerPage);

//# sourceMappingURL=manager.js.map

/***/ }),

/***/ 606:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(294);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(293);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(67);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    return MyApp;
}());
MyApp = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"C:\Users\USER\Documents\github\salecar\salecar\src\app\app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n\n\n\n\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\salecar\salecar\src\app\app.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
], MyApp);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 607:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FooterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_social_sharing__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_in_app_browser__ = __webpack_require__(295);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the FooterComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
var FooterComponent = (function () {
    function FooterComponent(socialSharing, iab) {
        this.socialSharing = socialSharing;
        this.iab = iab;
    }
    FooterComponent.prototype.Call = function () {
        window.location.href = 'tel:+97225654543';
    };
    FooterComponent.prototype.Facebook = function () {
        //window.location.href = 'https://www.facebook.com/dagimlabait/?fref=ts';
        this.browser = this.iab.create('https://www.facebook.com/dagimlabait/?fref=ts', '_blank', 'location=yes');
        //browser.executeScript(...);
        //browser.insertCSS(...);
        // browser.close();
    };
    FooterComponent.prototype.Mail = function () {
        window.location.href = 'mailto:Dan2772@gmail.com';
    };
    FooterComponent.prototype.Waze = function () {
        window.location.href = "waze://?q=31.766114,35.215068&navigate=yes";
    };
    FooterComponent.prototype.WhatsApp = function () {
        // Share via WhatsApp
        this.socialSharing.shareViaWhatsApp('גם אני הזמנתי דגים באפליקציית דגים עד הבית להורדה לחצו כאן', '', 'linktoandroidoriphone').then(function () {
            // Success!
        }).catch(function () {
            // Error!
        });
    };
    return FooterComponent;
}());
FooterComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'footer',template:/*ion-inline-start:"C:\Users\USER\Documents\github\salecar\salecar\src\components\footer\footer.html"*/'<!-- Generated template for the FooterComponent component -->\n\n\n\n  <ion-list>\n\n    <ion-icon class="icon"> <img src="images/main_icon_1.png" (click)="WhatsApp()" style="width: 100%;"/></ion-icon>\n\n    <ion-icon class="icon"> <img src="images/main_icon_2.png" (click)="Facebook()" style="width: 100%;"/></ion-icon>\n\n    <ion-icon class="icon"> <img src="images/main_icon_3.png" (click)="Call()" style="width: 100%;"/></ion-icon>\n\n    <ion-icon class="icon"> <img src="images/main_icon_4.png" (click)="Mail()" style="width: 100%;"/></ion-icon>\n\n    <ion-icon class="icon"> <img src="images/main_icon_5.png" (click)="Waze()"style="width: 100%;"/></ion-icon>\n\n  </ion-list>\n\n\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\salecar\salecar\src\components\footer\footer.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_native_social_sharing__["a" /* SocialSharing */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_in_app_browser__["a" /* InAppBrowser */]])
], FooterComponent);

//# sourceMappingURL=footer.js.map

/***/ }),

/***/ 608:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HeaderComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_basket_service__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_basket_basket__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_popups__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_home_home__ = __webpack_require__(67);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the HeaderComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
var HeaderComponent = (function () {
    function HeaderComponent(navCtrl, BasketService, zone, alertCtrl, Popup) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.BasketService = BasketService;
        this.zone = zone;
        this.alertCtrl = alertCtrl;
        this.Popup = Popup;
        this.BasketPrice = this.BasketService.BasketTotalPrice;
        this.BasketService.BasketTotalPrice$.subscribe(function (val) {
            _this.zone.run(function () { _this.BasketPrice = val; });
        });
    }
    HeaderComponent.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad header');
    };
    HeaderComponent.prototype.goToBasket = function () {
        if (this.BasketService.basket.length > 0)
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__pages_basket_basket__["a" /* BasketPage */]);
        else
            this.Popup.presentAlert('סל קניות ריק', 'יש תחילה להזמין מוצרים', '');
    };
    HeaderComponent.prototype.back = function () {
        this.Popup.presentAlert('סל קניות ריק', 'יש תחילה להזמין מוצרים', '');
    };
    HeaderComponent.prototype.goHome = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__pages_home_home__["a" /* HomePage */]);
    };
    HeaderComponent.prototype.ngOnInit = function () {
    };
    return HeaderComponent;
}());
HeaderComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'header',template:/*ion-inline-start:"C:\Users\USER\Documents\github\salecar\salecar\src\components\header\header.html"*/'\n\n\n\n\n\n<!-- Generated template for the HeaderComponent component -->\n\n\n\n<ion-toolbar no-border-top class="ToolBarClass1">\n\n\n\n\n\n  <ion-buttons left>\n\n    <button navPop style="background-color: black">\n\n      <ion-icon style="color: white; font-size: 30px;" name="ios-arrow-back-outline"></ion-icon>\n\n    </button>\n\n  </ion-buttons>\n\n\n\n  <div class="ToolBarTitle">\n\n    <ion-title>\n\n      <div (click)="goHome()">\n\n        <img src="images/logo1.png" style="width: 130px; margin-top: 4px;">\n\n      </div>\n\n    </ion-title>\n\n  </div>\n\n\n\n  <ion-buttons right>\n\n\n\n  </ion-buttons>\n\n\n\n\n\n</ion-toolbar>\n\n\n\n\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\salecar\salecar\src\components\header\header.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1__services_basket_service__["a" /* BasketService */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* NgZone */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_4__services_popups__["a" /* PopupService */]])
], HeaderComponent);

//# sourceMappingURL=header.js.map

/***/ }),

/***/ 609:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var UserService = (function () {
    function UserService(alertCtrl) {
        this.alertCtrl = alertCtrl;
    }
    ;
    UserService.prototype.logOut = function () {
        window.localStorage.userid = '';
        window.localStorage.name = '';
        //this.navCtrl.push(HomePage);
    };
    return UserService;
}());
UserService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* AlertController */]])
], UserService);

//# sourceMappingURL=user.js.map

/***/ }),

/***/ 67:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__companies_companies__ = __webpack_require__(154);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__login_login__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__company_admin_company_admin__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__single_single__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_CompanyService__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__manager_manager__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__single_car_single_car__ = __webpack_require__(162);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var HomePage = (function () {
    function HomePage(navCtrl, companyService) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.companyService = companyService;
        this.Global = 1;
        this.Details = {
            'company_id': '',
            'model': '',
            'manufacturer': '',
            'size': '',
            'year': '',
            'km': '',
            'color': '',
            'hand': '',
            'price': '',
            'description': ''
        };
        this.CarsTypesArray = [];
        this.CarsUpTo30 = [];
        this.Companies = [];
        this.host = "http://www.tapper.co.il/salecar/laravel/storage/app/public/";
        this.companyService.GetCarTypes('GetCarTypes').then(function (data) {
            _this.CarsTypesArray = _this.companyService.CarsTypesArray;
            console.log("CarTypes : ", data);
        });
        this.companyService.GetCarsUpTo30('GetCarsUpTo30').then(function (data) {
            console.log("Cars : ", data);
            _this.CarsUpTo30 = data;
        });
        this.companyService.GetAllCompanies('GetAllCompanies').then(function (data) {
            console.log("Basket : ", data), _this.Companies = data;
        });
        if (window['Global'] == 1) {
            this.companyService.GetCompanyBySubDomain('GetCompanyBySubDomain', window['SubDomain']).then(function (data) {
                console.log("SubDomain : ", data);
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__single_single__["a" /* SinglePage */], { CompanyId: data });
            });
        }
    }
    HomePage.prototype.GoToCompaniesPage = function () {
        //this.navCtrl.push(CompaniesPage);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__companies_companies__["a" /* CompaniesPage */]);
    };
    HomePage.prototype.GoToLoginPage = function () {
        if (window.localStorage.userid == '' || !window.localStorage.userid || window.localStorage.userid == 'undefined')
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__login_login__["a" /* LoginPage */]);
        else if (window.localStorage.isAdmin == 0)
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__company_admin_company_admin__["a" /* CompanyAdminPage */]);
        else
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__manager_manager__["a" /* ManagerPage */]);
    };
    HomePage.prototype.openCarPage = function (Id) {
        var id = this.CarsUpTo30[Id]['id'];
        console.log("T : ", id);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__single_car_single_car__["a" /* SingleCarPage */], { CarId: id });
    };
    HomePage.prototype.goToSinglePage = function (Id) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__single_single__["a" /* SinglePage */], { CompanyId: Id });
    };
    return HomePage;
}());
HomePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-home',template:/*ion-inline-start:"C:\Users\USER\Documents\github\salecar\salecar\src\pages\home\home.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <button ion-button menuToggle right>\n\n            <ion-icon name="menu">shay</ion-icon>\n\n        </button>\n\n        <ion-title>\n\n            <div>\n\n                <img src="images/logo1.png" style="width: 130px; margin-top: 4px;">\n\n            </div>\n\n        </ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content style="background-color: #f2f2f2">\n\n\n\n\n\n\n\n    <ion-list class="Inputs">\n\n        <ion-item padding>\n\n            <ion-label class="labelHome">בחר סוג רכב</ion-label>\n\n            <ion-select [(ngModel)]="Details.manufacturer" (ngModelChange)="CarTypeChange()">\n\n                <ion-option *ngFor="let type of CarsTypesArray" value="{{type.id}}" >{{type.name}}</ion-option>\n\n            </ion-select>\n\n        </ion-item>\n\n    </ion-list>\n\n\n\n    <ion-slides style="height: auto; margin-top: -37px;">\n\n        <ion-slide>\n\n            <img src="http://www.tapper.co.il/salecar/slider/1.png"/>\n\n        </ion-slide>\n\n        <ion-slide>\n\n            <img src="http://www.tapper.co.il/salecar/slider/2.png"/>\n\n        </ion-slide>\n\n        <ion-slide>\n\n            <img src="http://www.tapper.co.il/salecar/slider/3.png"/>\n\n        </ion-slide>\n\n        <ion-slide>\n\n            <img src="http://www.tapper.co.il/salecar/slider/4.png"/>\n\n        </ion-slide>\n\n        <ion-slide>\n\n            <img src="http://www.tapper.co.il/salecar/slider/5.png"/>\n\n        </ion-slide>\n\n    </ion-slides>\n\n\n\n    <div class="homeCatTitle" (click)="onGoToShopPage()">רכבי יוקרה</div>\n\n    <ion-scroll class="customScroll" scrollX="true">\n\n        <div text-center>\n\n            <div class="homeProduct video-block" *ngFor="let car of CarsUpTo30 let i=index" (click)="openCarPage(i)" >\n\n                <img *ngIf="car.images[0] != null" class="carImage" src="{{host}}/{{car.images[0].url}}" style="width:100%"/>\n\n                <div>\n\n                    <p class="Title">{{car.carName}}</p>\n\n                    <p class="homeTitle">{{car.ComapnyName}} </p>\n\n                    <p class="homeDescription">143 views </p>\n\n                </div>\n\n            </div>\n\n        </div>\n\n    </ion-scroll>\n\n\n\n    <div class="homeCatTitle" (click)="onGoToShopPage()" style="margin-top:-30px;" >רכבים עד 30,000 ש"ח</div>\n\n    <ion-scroll class="customScroll" scrollX="true">\n\n        <div text-center>\n\n            <div class="homeProduct video-block" *ngFor="let car of CarsUpTo30 let i=index" (click)="openCarPage(i)">\n\n                <img *ngIf="car.images[0] != null" class="carImage" src="{{host}}/{{car.images[0].url}}" style="width:100%"/>\n\n                <div>\n\n                    <p class="Title">{{car.carName}}</p>\n\n                    <p class="homeTitle">{{car.ComapnyName}} </p>\n\n                    <p class="homeDescription">143 views </p>\n\n                </div>\n\n            </div>\n\n        </div>\n\n    </ion-scroll>\n\n\n\n\n\n    <div class="homeCatTitle" (click)="onGoToShopPage()" style="margin-top:-30px;">מגרשי מכוניות</div>\n\n    <ion-scroll class="customScroll" scrollX="true">\n\n        <div text-center>\n\n            <div class="homeProduct video-block" *ngFor="let company of Companies let i=index" (click)="goToSinglePage(i)">\n\n                <img *ngIf="company.logo != null"  src="{{host}}/{{company.logo}}" style="width:100%; margin-top:-3px;"  />\n\n                <img *ngIf="company.logo == null"  src="images/company.png" style="width:100%; margin-top:-3px;"  />\n\n                <div>\n\n                    <p class="Title">{{company.name}}</p>\n\n                   <!-- <p class="homeTitle">{{company.ComapnyName}} </p> -->\n\n                    <p class="homeDescription">143 views </p>\n\n                </div>\n\n            </div>\n\n        </div>\n\n    </ion-scroll>\n\n\n\n\n\n   <!--<img src="images/main1.png" (click)="GoToCompaniesPage()" style="width: 100%"/>\n\n    <img src="images/main2.png" style="width: 100%"/>\n\n    <img src="images/main3.png" style="width: 100%"/> -->\n\n    <img src="images/contact.png" style="width: 100%" (click)="GoToLoginPage()"/>\n\n</ion-content>\n\n\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\salecar\salecar\src\pages\home\home.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_6__services_CompanyService__["a" /* CompanyService */]])
], HomePage);

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 68:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SinglePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_basket_service__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_config__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_popups__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng2_translate__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_CompanyService__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__car_car__ = __webpack_require__(155);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_social_sharing__ = __webpack_require__(88);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









/**
 * Generated class for the SinglePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var SinglePage = (function () {
    function SinglePage(navCtrl, socialSharing, navParams, toastCtrl, companyService, alertCtrl, Settings, BasketService, Popup, translate) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.socialSharing = socialSharing;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.companyService = companyService;
        this.alertCtrl = alertCtrl;
        this.Settings = Settings;
        this.BasketService = BasketService;
        this.Popup = Popup;
        this.translate = translate;
        this.Company = [];
        this.Cars = [];
        this.CompanyId = '';
        this.CompanyPlace = '';
        //public host = "http://www.tapper.co.il/salecar/admin/public"
        this.host = "http://www.tapper.co.il/salecar/laravel/storage/app/public/";
        this.message = 'הזמין אותך לצפות בכרטיס ביקור המכיל את כל הרכבים שלו לצפייה לחץ כאן :';
        this.link = '';
        this.fullMessage = '';
        this.fullImage = '';
        if (window['Global'] == 1)
            this.Company = navParams.get('CompanyId');
        else {
            this.CompanyPlace = navParams.get('CompanyId');
            this.Company = this.companyService.CompaniesArray[this.CompanyPlace];
        }
        this.CompanyId = this.Company['id'];
        this.companyService.GetAllCars('GetAllCars', this.CompanyId).then(function (data) {
            console.log("Basket : ", data), _this.Cars = data;
        });
        this.link = "http://" + this.Company['subdomain'] + ".salecar.co.il";
        this.fullMessage = this.Company['name'] + this.message;
        this.fullImage = this.host + "/" + this.Company['logo'];
        console.log(this.fullMessage);
        console.log(this.Company['id']);
    }
    SinglePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SinglePage');
    };
    SinglePage.prototype.goToCarPage = function (Id) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__car_car__["a" /* CarPage */], { CarPlace: Id });
    };
    SinglePage.prototype.Face = function () {
        if (this.Company['facebook'] != null) {
            window.location.href = this.Company['facebook'];
        }
        else {
            this.presentToast('ללקוח זה לא קיים פייסבוק');
        }
    };
    SinglePage.prototype.waze = function () {
        if (this.Company['waze'] != null) {
            window.location.href = "waze://?ll=" + this.Company['waze'];
        }
        else {
            this.presentToast('ללקוח זה לא קיים WAZE');
        }
    };
    SinglePage.prototype.site = function () {
        if (this.Company['website'] != null) {
            window.location.href = this.Company['website'];
        }
        else {
            this.presentToast('ללקוח זה לא קיים אתר אינטרנט');
        }
    };
    SinglePage.prototype.tel = function () {
        if (this.Company['phone'] != null) {
            window.location.href = "tel:" + this.Company['phone'];
        }
        else {
            this.presentToast('ללקוח זה לא קיים טלפון במערכת');
        }
    };
    SinglePage.prototype.mail = function () {
        if (this.Company['email'] != null) {
            window.location.href = "mailto:" + this.Company['email'];
        }
        else {
            this.presentToast('ללקוח זה לא קיים אימייל');
        }
    };
    SinglePage.prototype.share = function () {
        this.socialSharing.shareViaWhatsApp(this.fullMessage, this.fullImage, this.link).then(function () {
            // Success!
        }).catch(function () {
            // Error!
        });
    };
    SinglePage.prototype.presentToast = function (text) {
        console.log("f3");
        var toast = this.toastCtrl.create({
            message: text,
            duration: 3000,
            position: 'bottom',
            cssClass: "ToastClass"
        });
        toast.present();
    };
    return SinglePage;
}());
SinglePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-single',template:/*ion-inline-start:"C:\Users\USER\Documents\github\salecar\salecar\src\pages\single\single.html"*/'<ion-header>\n\n    <header></header>\n\n</ion-header>\n\n\n\n<ion-content class="Content" style="margin-top: 0px">\n\n    <div class="singleName">\n\n        {{Company.name}}\n\n    </div>\n\n   <!-- <ion-slides *ngIf="Company.logo != null" style="height:auto">\n\n        <ion-slide>\n\n            <img src="http://www.tapper.co.il/salecar/companies/{{CompanyId}}/1.jpg"/>\n\n        </ion-slide>\n\n        <ion-slide>\n\n            <img src="http://www.tapper.co.il/salecar/companies/{{CompanyId}}/2.jpg"/>\n\n        </ion-slide>\n\n        <ion-slide>\n\n            <img src="http://www.tapper.co.il/salecar/companies/{{CompanyId}}/3.jpg"/>\n\n        </ion-slide>\n\n    </ion-slides>-->\n\n    <img *ngIf="Company.logo != null" src="http://www.tapper.co.il/salecar/companies/{{CompanyId}}/1.jpg"/>\n\n    <img *ngIf="Company.logo == null" src="images/main_image.png"/>\n\n\n\n    <div class="SocialIcons">\n\n        <div class="SocialIconsDiv">\n\n            <img src="images/main_icons/inst.png"/>\n\n        </div>\n\n        <div class="SocialIconsDiv" (click)="Face()">\n\n            <img src="images/main_icons/face.png" class="w100"/>\n\n        </div>\n\n        <div class="SocialIconsDiv" (click)="waze()">\n\n            <img src="images/main_icons/waze.png" class="w100"/>\n\n        </div>\n\n    </div>\n\n\n\n    <div class="SocialIcons">\n\n        <div class="MainIconsDiv" (click)="site()">\n\n            <img src="images/main_icons/ic2.png"/>\n\n        </div>\n\n        <div class="MainIconsDiv" (click)="tel()">\n\n            <img src="images/main_icons/ic4.png" class="w100"/>\n\n        </div>\n\n        <div class="MainIconsDiv" (click)="mail()">\n\n            <img src="images/main_icons/ic3.png" class="w100"/>\n\n        </div>\n\n        <div class="MainIconsDiv" (click)="share()">\n\n            <img src="images/main_icons/ic1.png" class="w100"/>\n\n        </div>\n\n    </div>\n\n\n\n    <div class="Info">\n\n        <div class="Logo" align="center">\n\n            <div class="w80">\n\n                <img *ngIf="Company.logo != null" src="{{host}}/{{Company.logo}}" style="width:100%; margin-top:-3px;"  />\n\n                <img *ngIf="Company.logo == null" src="images/logos.png"/>\n\n            </div>\n\n        </div>\n\n        <div class="Details">\n\n            <p>{{Company.name}}</p>\n\n            <h3>{{Company.address}}</h3>\n\n            <h4>טלפון : {{Company.phone}}</h4>\n\n        </div>\n\n    </div>\n\n\n\n    <div class="c3Title">\n\n        על העסק\n\n    </div>\n\n    <div style="width:100%" align="center">\n\n        <div class="c3Line">\n\n        </div>\n\n    </div>\n\n    <div align="center">\n\n        <div class="c3Info">\n\n            {{Company.description}}\n\n        </div>\n\n    </div>\n\n\n\n\n\n    <!-- Cars -->\n\n    <div>\n\n        <div *ngFor="let car of Cars let i = index"  >\n\n            <div class="ListRow" (click)="goToCarPage(i)">\n\n                <div class="ListRight"  >\n\n                    <img *ngIf="car.images[0] != null" src="{{host}}{{car.images[0].url}}" style="width:100%; margin-top:-3px;"  />\n\n                    <img *ngIf="car.images[0] == null" src="{{host}}/{{Company.logo}}" style="width:100%; margin-top:-3px;"  />\n\n                </div>\n\n                <div class="ListCenter"  >\n\n                    <p class="bName">{{car.carName}}</p>\n\n                    <h3> {{car.carModelName}}</h3>\n\n                    <h4 style="direction: rtl">שנה : {{car.year}} </h4>\n\n                </div>\n\n\n\n                <div class="ListLeft">\n\n                    <div>\n\n                        <img  src="images/arrow.png" style="width:20px;" />\n\n                    </div>\n\n                </div>\n\n            </div>\n\n\n\n            <div>\n\n                <img  src="images/line.png" style="width:100%; margin-top:-3px;" />\n\n            </div>\n\n        </div>\n\n    </div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\salecar\salecar\src\pages\single\single.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_8__ionic_native_social_sharing__["a" /* SocialSharing */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* ToastController */], __WEBPACK_IMPORTED_MODULE_6__services_CompanyService__["a" /* CompanyService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_3__services_config__["a" /* Config */], __WEBPACK_IMPORTED_MODULE_2__services_basket_service__["a" /* BasketService */], __WEBPACK_IMPORTED_MODULE_4__services_popups__["a" /* PopupService */], __WEBPACK_IMPORTED_MODULE_5_ng2_translate__["c" /* TranslateService */]])
], SinglePage);

//# sourceMappingURL=single.js.map

/***/ }),

/***/ 93:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BasketPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_send_to_server__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_basket_service__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__popup_popup__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__home_home__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_config__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__login_login__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__services_popups__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_ng2_translate__ = __webpack_require__(48);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










/**
 * Generated class for the BasketPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var BasketPage = (function () {
    function BasketPage(navCtrl, zone, navParams, Server, BasketService, modalCtrl, Settings, alertCtrl, Popup, translate) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.zone = zone;
        this.navParams = navParams;
        this.Server = Server;
        this.BasketService = BasketService;
        this.modalCtrl = modalCtrl;
        this.Settings = Settings;
        this.alertCtrl = alertCtrl;
        this.Popup = Popup;
        this.translate = translate;
        this.isAvaileble = false;
        this.TotalPrice = 0;
        this.BasketPrice = 0;
        this.defaultLangage = '';
        this.defaultLangage = this.Settings.defaultLanguage;
        translate.onLangChange.subscribe(function (event) {
        });
        this.BasketService.BasketTotalPrice$.subscribe(function (val) {
            _this.zone.run(function () { _this.BasketPrice = val; });
        });
    }
    BasketPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad BasketPage');
    };
    BasketPage.prototype.ngOnInit = function () {
        this.ServerHost = this.Settings.ServerHost;
        this.products = this.BasketService.basket;
        this.TotalPrice = 0;
        for (var i = 0; i < this.products.length; i++) {
            this.TotalPrice += Number(this.products[i].Qnt) * Number(this.products[i].price);
        }
        this.CalculateBasket();
    };
    BasketPage.prototype.deleteProduct = function (index) {
        this.BasketService.deleteProduct(index);
    };
    BasketPage.prototype.SendOrderToServer = function () {
        var _this = this;
        this.Server.SendBasketToServer('getBasket', this.products, this.TotalPrice, this.Pay).then(function (data) { console.log("Basket : ", data), _this.showPopup(); });
    };
    BasketPage.prototype.checkLoggedIn = function () {
        if (this.checkBasket() == true) {
            if (window.localStorage.userid) {
                if (this.Pay == '' || this.Pay == undefined)
                    this.Popup.presentAlert(this.translate.instant("selectpaytypetext"), this.translate.instant("selectpayoptiontext"), '');
                else {
                    this.SendOrderToServer();
                    this.BasketService.resetTotalPrice();
                }
            }
            else
                this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__login_login__["a" /* LoginPage */]);
        }
    };
    BasketPage.prototype.checkBasket = function () {
        if (this.products.length == 0) {
            this.Popup.presentAlert(this.translate.instant("emptybaskettext"), this.translate.instant("addproductstext"), '');
            return false;
        }
        else
            return true;
    };
    BasketPage.prototype.CalculateBasket = function () {
        this.BasketPrice = this.BasketService.BasketTotalPrice;
        console.log("BasketPrice basket", this.BasketPrice);
    };
    BasketPage.prototype.showPopup = function () {
        var _this = this;
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__popup_popup__["a" /* PopupPage */], { url: this.translate.instant("basketpopupimage") }, { cssClass: 'FBPage' });
        modal.present();
        modal.onDidDismiss(function (data) { _this.BasketService.emptyBasket(), _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__home_home__["a" /* HomePage */]); });
    };
    return BasketPage;
}());
BasketPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-basket',template:/*ion-inline-start:"C:\Users\USER\Documents\github\salecar\salecar\src\pages\basket\basket.html"*/'\n\n<ion-header>\n\n  <header></header>\n\n</ion-header>\n\n\n\n\n\n\n\n\n\n<ion-content padding>\n\n  <div class="Product" *ngFor="let product of products let i=index">\n\n    <div class="ProductRight">\n\n      <img src="{{ServerHost+product.image}}" class="w100"  />\n\n    </div>\n\n\n\n    <div class="ProductCenter">\n\n      <span *ngIf="defaultLangage == \'he\'">{{product.name}}</span>\n\n      <span *ngIf="defaultLangage == \'en\'">{{product.name_english}}</span>\n\n      <p>{{product.Qnt}} {{\'kilotext\' | translate}} &nbsp; </p>\n\n    </div>\n\n\n\n    <div class="ProductCenter1">\n\n      <p> {{product.price}}<span>{{\'nistext\' | translate}}</span> &nbsp; </p>\n\n    </div>\n\n\n\n    <div class="ProductLeft" (click)="deleteProduct(i)">\n\n      <ion-icon name="ios-trash-outline"></ion-icon>\n\n    </div>\n\n  </div>\n\n\n\n  <ion-list radio-group [(ngModel)]="Pay" class="CB item-borderless" no-lines>\n\n    <ion-item class="CBright" no-lines>\n\n      <ion-label item-right class="CoLabel"> {{\'credittext\' | translate}}</ion-label>\n\n      <ion-radio  item-right value="1" name="1" class="CoLabel" checked></ion-radio>\n\n    </ion-item>\n\n    <ion-item class="CBright" no-lines>\n\n      <ion-label item-right class="CoLabel">{{\'cashtext\' | translate}}</ion-label>\n\n      <ion-radio  item-right value="0" name="0" class="CoLabel" [disabled]="isDisabled"></ion-radio>\n\n    </ion-item>\n\n  </ion-list>\n\n\n\n\n\n</ion-content>\n\n\n\n\n\n<ion-footer style="background-color: darkblue !important; height: 130px; ">\n\n  <div class="inviteFooter" (click)="checkLoggedIn()">\n\n      <div class="inviteFooterRight">\n\n        <span >{{\'totaltext\' | translate}} :</span> {{BasketPrice}} <span >{{\'nistext\' | translate}}</span>\n\n      </div>\n\n      <div class="inviteFooterLeft" >\n\n        <img src="{{\'addtobasketimage\' | translate}}" class="w100"  />\n\n      </div>\n\n  </div>\n\n  <footer></footer>\n\n</ion-footer>\n\n\n\n\n\n\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\salecar\salecar\src\pages\basket\basket.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* NgZone */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_send_to_server__["a" /* SendToServerService */], __WEBPACK_IMPORTED_MODULE_3__services_basket_service__["a" /* BasketService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ModalController */], __WEBPACK_IMPORTED_MODULE_6__services_config__["a" /* Config */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_8__services_popups__["a" /* PopupService */], __WEBPACK_IMPORTED_MODULE_9_ng2_translate__["c" /* TranslateService */]])
], BasketPage);

//# sourceMappingURL=basket.js.map

/***/ }),

/***/ 94:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ImageCarPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_diagnostic__ = __webpack_require__(251);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_file_transfer__ = __webpack_require__(252);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_camera_service__ = __webpack_require__(253);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_file__ = __webpack_require__(127);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_transfer__ = __webpack_require__(128);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_file_path__ = __webpack_require__(129);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_camera__ = __webpack_require__(130);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__services_CompanyService__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__company_admin_company_admin__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__popup_popup__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};













var ImageCarPage = (function () {
    //f2 : file:///storage/emulated/0/Android/data/io.ionic.starter18/cache/ : .Pic.jpg : 1510223388621.jpg
    //f2 : file:///storage/emulated/0/Android/data/io.ionic.starter18/cache/ : .Pic.jpg : 1510223666791.jpg
    function ImageCarPage(navCtrl, modalCtrl, companyService, navParams, cameraService, diagnostic, Ftransfer, camera, transfer, file, filePath, actionSheetCtrl, toastCtrl, platform, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl;
        this.companyService = companyService;
        this.navParams = navParams;
        this.cameraService = cameraService;
        this.diagnostic = diagnostic;
        this.Ftransfer = Ftransfer;
        this.camera = camera;
        this.transfer = transfer;
        this.file = file;
        this.filePath = filePath;
        this.actionSheetCtrl = actionSheetCtrl;
        this.toastCtrl = toastCtrl;
        this.platform = platform;
        this.loadingCtrl = loadingCtrl;
        this.CarImages = [];
        this.host = "http://www.tapper.co.il/salecar/laravel/storage/app/public/";
        this.lastImage = null;
        this.Car = navParams.get('Car');
        this.Type = navParams.get('Type');
        console.log("Car : ", this.Car);
        if (this.Type == 0)
            this.CarImages = this.Car['images'];
        else {
            this.CarImages = this.Car[0]['images'];
            this.Car = this.Car[0];
        }
        console.log("Img : ", this.CarImages);
    }
    ImageCarPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ImageCarPage');
    };
    ImageCarPage.prototype.goToCarAdmin = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_10__company_admin_company_admin__["a" /* CompanyAdminPage */]);
    };
    ImageCarPage.prototype.delImage = function (i) {
        var _this = this;
        this.companyService.DelImageCar('DelImageCar', this.Car['id'], this.CarImages[i].id).then(function (data) {
            console.log("R : ", data);
            _this.CarImages = data;
        });
    };
    ImageCarPage.prototype.showPopup = function (i) {
        var Url = this.host + '' + this.CarImages[i].url;
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_11__popup_popup__["a" /* PopupPage */], { url: Url }, { cssClass: 'FBPage' });
        modal.present();
        modal.onDidDismiss(function (data) { });
    };
    ImageCarPage.prototype.presentActionSheet = function () {
        var _this = this;
        if (this.CarImages.length < 6) {
            var actionSheet = this.actionSheetCtrl.create({
                title: 'Select Image Source',
                buttons: [
                    {
                        text: 'Load from Library',
                        handler: function () {
                            _this.takePicture(_this.camera.PictureSourceType.PHOTOLIBRARY);
                        }
                    },
                    {
                        text: 'Use Camera',
                        handler: function () {
                            _this.takePicture(_this.camera.PictureSourceType.CAMERA);
                        }
                    },
                    {
                        text: 'Cancel',
                        role: 'cancel'
                    }
                ]
            });
            actionSheet.present();
        }
        else
            this.presentToast('אין אפשרות להוסיף יותר מ6 תמונות');
    };
    ImageCarPage.prototype.takePicture = function (sourceType) {
        var _this = this;
        // Create options for the Camera Dialog
        var options = {
            quality: 60,
            sourceType: sourceType,
            saveToPhotoAlbum: false,
            correctOrientation: true,
            targetWidth: 1000,
            targetHeight: 1000,
            allowEdit: true
        };
        // Get the data of an image
        this.camera.getPicture(options).then(function (imagePath) {
            // Special handling for Android library
            console.log("f0");
            if (_this.platform.is('android') && sourceType === _this.camera.PictureSourceType.PHOTOLIBRARY) {
                _this.filePath.resolveNativePath(imagePath)
                    .then(function (filePath) {
                    console.log("f01");
                    var correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                    console.log("f02");
                    var currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                    console.log("f03");
                    _this.copyFileToLocalDir(correctPath, currentName, _this.createFileName());
                });
            }
            else {
                var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
                var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
                _this.copyFileToLocalDir(correctPath, currentName, _this.createFileName());
            }
        }, function (err) {
            _this.presentToast('Error while selecting image.');
        });
    };
    // Create a new name for the image
    ImageCarPage.prototype.createFileName = function () {
        console.log("f1");
        var d = new Date(), n = d.getTime(), newFileName = n + ".jpg";
        return newFileName;
    };
    // Copy the image to a local folder
    ImageCarPage.prototype.copyFileToLocalDir = function (namePath, currentName, newFileName) {
        var _this = this;
        console.log("f2 : " + namePath + " : " + currentName + " : " + newFileName);
        this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(function (success) {
            _this.lastImage = newFileName;
            _this.uploadImage();
        }, function (error) {
            _this.presentToast('Error while storing file.');
        });
    };
    ImageCarPage.prototype.presentToast = function (text) {
        console.log("f3");
        var toast = this.toastCtrl.create({
            message: text,
            duration: 3000,
            position: 'bottom',
            cssClass: "ToastClass"
        });
        toast.present();
    };
    // Always get the accurate path to your apps folder
    ImageCarPage.prototype.pathForImage = function (img) {
        if (img === null) {
            return '';
        }
        else {
            return cordova.file.dataDirectory + img;
        }
    };
    ImageCarPage.prototype.uploadImage = function () {
        var _this = this;
        // Destination URL
        console.log("Up1  : ");
        var url = "http://www.tapper.co.il/salecar/laravel/public/api/GetFile";
        // File for Upload
        console.log("Up2  : " + this.lastImage + " : " + this.pathForImage(this.lastImage));
        var targetPath = this.pathForImage(this.lastImage);
        // File name only
        var filename = this.lastImage;
        var options = {
            fileKey: "file",
            fileName: filename,
            chunkedMode: false,
            mimeType: "multipart/form-data",
            params: { 'fileName': filename, 'CarId': this.Car['id'] }
        };
        console.log("Up3  : " + this.Car['id']);
        var fileTransfer = this.transfer.create();
        this.loading = this.loadingCtrl.create({
            content: 'Uploading...',
        });
        this.loading.present();
        // Use the FileTransfer to upload the image
        fileTransfer.upload(targetPath, url, options).then(function (data) {
            _this.loading.dismissAll();
            _this.callToImagesFromServer(data);
        }, function (err) {
            _this.loading.dismissAll();
            _this.presentToast('Error while uploading file.');
        });
    };
    ImageCarPage.prototype.callToImagesFromServer = function (data) {
        console.log("GT : ", JSON.parse(data.response));
        this.CarImages = JSON.parse(data.response);
        //console.log(this.companyService.CompaniesArray);
        /* this.companyService.GetCompanyById('GetCompanyById').then((data: any) => {
             console.log("CarAdmin : ", data);
             this.Car = this.companyService.CompanyAdmin['cars'][this.CarPlace];
             this.CarImages = this.Car['images'];
         });*/
    };
    return ImageCarPage;
}());
ImageCarPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-image-car',template:/*ion-inline-start:"C:\Users\USER\Documents\github\salecar\salecar\src\pages\image-car\image-car.html"*/'<ion-header>\n\n  <header></header>\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n  <div class="singleName">\n\n    {{Car.carModelName}} &nbsp; {{Car.carName}}\n\n  </div>\n\n  <div class="CarImages">\n\n    <div class="CarImageDiv" *ngFor="let image of CarImages let i = index" >\n\n        <img class="delImage" src="images/close.png" (click)="delImage(i)"/>\n\n      <img class="carImage"  *ngIf="i<16" src="{{host}}/{{image.url}}" (click)="showPopup(i)"/>\n\n    </div>\n\n  </div>\n\n\n\n  <div style="width: 100%; margin-top: 20px;" align="center" (click)="presentActionSheet()">\n\n    <div style="width: 90%">\n\n      <button ion-button color="primary" style="width: 100%; color: black">הוסף תמונה</button>\n\n    </div>\n\n  </div>\n\n\n\n    <div style="width: 100%; margin-top:0px;" align="center" (click)="goToCarAdmin()">\n\n        <div style="width: 90%">\n\n            <button ion-button color="#666666" style="width: 100%; color: white; background-color: #000">סיים</button>\n\n        </div>\n\n    </div>\n\n</ion-content>\n\n\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\salecar\salecar\src\pages\image-car\image-car.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ModalController */], __WEBPACK_IMPORTED_MODULE_9__services_CompanyService__["a" /* CompanyService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_4__services_camera_service__["a" /* CameraService */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_diagnostic__["a" /* Diagnostic */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_file_transfer__["a" /* FileTransfer */], __WEBPACK_IMPORTED_MODULE_8__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_transfer__["a" /* Transfer */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_file__["a" /* File */], __WEBPACK_IMPORTED_MODULE_7__ionic_native_file_path__["a" /* FilePath */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* ToastController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */]])
], ImageCarPage);

/*public presentActionSheet() {
       let actionSheet = this.actionSheetCtrl.create({
           title: 'Select Image Source',
           buttons: [
               {
                   text: 'Load from Library',
                   handler: () => {
                       this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
                   }
               },
               {
                   text: 'Use Camera',
                   handler: () => {
                       this.takePicture(this.camera.PictureSourceType.CAMERA);
                   }
               },
               {
                   text: 'Cancel',
                   role: 'cancel'
               }
           ]
       });
       actionSheet.present();
   }

   public takePicture(sourceType) {
       // Create options for the Camera Dialog
       var options = {
           quality: 60,
           sourceType: sourceType,
           saveToPhotoAlbum: false,
           correctOrientation: true,
           targetWidth: 1000,
           targetHeight: 1000,
           allowEdit: true
       };

       // Get the data of an image
       this.camera.getPicture(options).then((imagePath) => {
           // Special handling for Android library
           if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
               this.filePath.resolveNativePath(imagePath)
                   .then(filePath => {
                       let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                       let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                       this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
                   });
           } else {
               var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
               var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
               this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
           }

       }, (err) => {
           this.presentToast('Error while selecting image.');
       });
   }


   // Create a new name for the image
   private createFileName() {
       var d = new Date(),
           n = d.getTime(),
           newFileName =  n + ".jpg";
       return newFileName;
   }

// Copy the image to a local folder
   private copyFileToLocalDir(namePath, currentName, newFileName) {
       this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
           this.lastImage = newFileName;
           this.uploadImage();
       }, error => {
           this.presentToast('Error while storing file.');
       });
   }

   private presentToast(text) {
       let toast = this.toastCtrl.create({
           message: text,
           duration: 3000,
           position: 'top'
       });
       toast.present();
   }

// Always get the accurate path to your apps folder
   public pathForImage(img) {
       if (img === null) {
           return '';
       } else {
           return cordova.file.dataDirectory + img;
       }
   }

   public uploadImage() {
       // Destination URL
       var url = "http://www.tapper.co.il/salecar/laravel/public/api/GetFile";

       // File for Upload
       var targetPath = this.pathForImage(this.lastImage);

       // File name only
       var filename = this.lastImage;

       var options = {
           fileKey: "file",
           fileName: filename,
           chunkedMode: false,
           mimeType: "multipart/form-data",
           params : {'fileName': filename, 'CarId':this.Car['id']}
       };

       const fileTransfer: TransferObject = this.transfer.create();

       this.loading = this.loadingCtrl.create({
           content: 'Uploading...',
       });
       this.loading.present();

       // Use the FileTransfer to upload the image
       fileTransfer.upload(targetPath, url, options).then(data => {
           this.loading.dismissAll()
           this.companyService.GetCompanyById('GetCompanyById').then((data: any) => {
               console.log("CarAdmin : ", data);
           });
           /*this.presentToast('Image succesful uploaded.');
           this.Car['images'] = data.response;
           this.CarImages = this.Car['images'];
           alert(JSON.stringify(data.response));
       }, err => {
           this.loading.dismissAll()
           this.presentToast('Error while uploading file.');
       });
   }*/
//# sourceMappingURL=image-car.js.map

/***/ })

},[299]);
//# sourceMappingURL=main.js.map